package com.stbig.radits.task;

import android.content.Context;
import android.util.Log;

import com.spoledge.aacdecoder.MultiPlayer;
import com.spoledge.aacdecoder.PlayerCallback;
import com.stbig.radits.R;

/**
 * Created by root on 11/02/16.
 */
public class AACTask implements Runnable {

    private boolean playing;
    private Context context;
    private MultiPlayer aacMp3Player=null;
    private PlayerCallback clb;
    private String source;

    public AACTask(Context context){
        this.context=context;
    }

    public void terminate(){
        playing =false;
    }

    @Override
    public void run() {

        while (playing) {
            try {
                aacMp3Player = new MultiPlayer(clb);
                aacMp3Player.playAsync(source);
            } catch (Exception e) {
                Log.e("Exception", e.getMessage());
                playing = false;
            }
        }

    }
}
