package com.stbig.radits;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;
import com.stbig.radits.datos.AppPreferences;
import com.stbig.radits.fragments.FragmentContacto;
import com.stbig.radits.fragments.FragmentDeportes;
import com.stbig.radits.fragments.FragmentEscuchados;
import com.stbig.radits.fragments.FragmentFavoritos;
import com.stbig.radits.fragments.FragmentGrabaciones;
import com.stbig.radits.fragments.FragmentMusica;
import com.stbig.radits.fragments.FragmentNoticias;
import com.stbig.radits.fragments.FragmentPaises;
import com.stbig.radits.fragments.FragmentRecientes;
import com.stbig.radits.fragments.MainFragment;
import com.stbig.radits.fragments.ViewPagerFragment;
import com.stbig.radits.helper.ManagerModel;
import com.stbig.radits.helper.PlayerConstants;
import com.stbig.radits.helper.imagen.Utils;
import com.stbig.radits.listener.ItemClickGeneroSupport;
import com.stbig.radits.objects.Extra;
import com.stbig.radits.objects.Radio;
import com.stbig.radits.services.APlayerService;


public class MainActivity extends AppCompatActivity implements ItemClickGeneroSupport, View.OnClickListener, Drawer.OnDrawerListener {

    private Drawer result;
    private static Fragment mFragment;
    private int modeFragmentType=0;
    static AppCompatActivity activity;
    static FragmentManager mFragmentManager;
    private FrameLayout container;


    private AppPreferences preferences;


    public static ActionBarDrawerToggle toggle;
    private Intent acpServices;
    private ManagerModel model;

    private Radio currentRadio;



    private ImageView btnStop;
    private static ImageView btnLike;
    private ImageView radioImage;
    private ImageView radioIcon;
    private TextView radioName;
    private AnimationDrawable frameAnimation;
    private ImageView animate;
    public static SlidingUpPanelLayout mediaScreen;

    private float scale;
    private int padding_70dp;
    private String title="";
    boolean modeList = false;

    private static ViewPagerFragment vf;



    private MenuItem menItemMode;
    private Menu menu = null;

    private AdView mAdViewPlayer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        scale = getResources().getDisplayMetrics().density;
        padding_70dp = (int) (70 * scale + 0.5f);

        activity = MainActivity.this;
        mFragmentManager = getSupportFragmentManager();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        container = (FrameLayout) findViewById(R.id.drawer_layout);
        btnStop = (ImageView) findViewById(R.id.stop);
        btnLike = (ImageView) findViewById(R.id.like);
        radioImage = (ImageView) findViewById(R.id.image_radio);
        radioIcon = (ImageView) findViewById(R.id.icon_radio);
        radioName = (TextView) findViewById(R.id.song_name);
        animate = (ImageView)  findViewById(R.id.play_animation);
        mediaScreen = (SlidingUpPanelLayout) findViewById(R.id.media_screen);


        getSupportActionBar().setElevation(0);

        btnStop.setOnClickListener(this);
        btnLike.setOnClickListener(this);

        model = new ManagerModel(this);
        preferences = new AppPreferences(this);

        acpServices = new Intent(this, APlayerService.class);

        modeList = preferences.getValueBoolean(getString(R.string.mode_list));

        PrimaryDrawerItem item1 = new PrimaryDrawerItem().withName(getString(R.string.nav_1)).withIcon(R.drawable.ic_radioslocales);
        SecondaryDrawerItem item2 = new SecondaryDrawerItem().withName(getString(R.string.nav_2)).withIcon(R.drawable.ic_favorito_off);
        SecondaryDrawerItem item3 = new SecondaryDrawerItem().withName(getString(R.string.nav_3_3)).withIcon(R.drawable.ic_recientemente);
        SecondaryDrawerItem item4 = new SecondaryDrawerItem().withName(getString(R.string.nav_4)).withIcon(R.drawable.ic_lomasescuchado);
        SecondaryDrawerItem item5 = new SecondaryDrawerItem().withName(getString(R.string.nav_5)).withIcon(R.drawable.ic_paises);
        SecondaryDrawerItem item6 = new SecondaryDrawerItem().withName(getString(R.string.nav_6)).withIcon(R.drawable.ic_musica);
        SecondaryDrawerItem item7 = new SecondaryDrawerItem().withName(getString(R.string.nav_7)).withIcon(R.drawable.ic_noticias);
        SecondaryDrawerItem item8 = new SecondaryDrawerItem().withName(getString(R.string.nav_8)).withIcon(R.drawable.ic_deportes);
        SecondaryDrawerItem item9 = new SecondaryDrawerItem().withName(getString(R.string.nav_9)).withIcon(R.drawable.ic_grabaciones);
        SecondaryDrawerItem item10 = new SecondaryDrawerItem().withName(getString(R.string.nav_10)).withIcon(R.drawable.ic_contact);



        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.ic_user_background)
                .withSelectionListEnabledForSingleProfile(false)
                .addProfiles(
                        new ProfileDrawerItem().withName("Helbert Medina").withEmail("hsmedinar@gmail.com").withIcon(getResources().getDrawable(R.drawable.ic_user))
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }
                })
                .build();


        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withSelectedItemByPosition(1)
                .withActionBarDrawerToggle(true)
                .withActionBarDrawerToggleAnimated(true)
                .withAccountHeader(headerResult)
                .withOnDrawerListener(this)
                .addDrawerItems(
                        item1,
                        new DividerDrawerItem(),
                        item2,
                        item3,
                        item4,
                        item5,
                        item6,
                        item7,
                        item8,
                        item9,
                        item10
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {

                        Bundle bundle = new Bundle();

                        switch (position) {
                            case 1:
                                mFragment = new ViewPagerFragment();
                                bundle.putInt("page", 0);
                                mFragment.setArguments(bundle);
                                title = getString(R.string.nav_1);
                                modeFragmentType=0;
                                break;
                            case 3:
                                mFragment = new ViewPagerFragment();
                                bundle.putInt("page", 1);
                                mFragment.setArguments(bundle);
                                title = getString(R.string.nav_2);
                                modeFragmentType=0;
                                break;
                            case 4:
                                mFragment = new ViewPagerFragment();
                                bundle.putInt("page", 2);
                                mFragment.setArguments(bundle);
                                title = getString(R.string.nav_3_3);
                                modeFragmentType=0;
                                break;
                            case 5:
                                mFragment = new FragmentEscuchados();
                                title = getString(R.string.nav_4);
                                break;
                            case 6:
                                mFragment = new FragmentPaises();
                                title = getString(R.string.nav_5_1);
                                break;
                            case 7:
                                mFragment = new FragmentMusica();
                                title = getString(R.string.nav_6);
                                break;
                            case 8:
                                mFragment = new FragmentNoticias();
                                title = getString(R.string.nav_7);
                                break;
                            case 9:
                                mFragment = new FragmentDeportes();
                                title = getString(R.string.nav_8);
                                break;
                            case 10:
                                mFragment = new FragmentGrabaciones();
                                title = getString(R.string.nav_9);
                                break;
                            case 11:
                                mFragment = new FragmentContacto();
                                title = getString(R.string.nav_10);
                                break;
                            default:
                                title = getString(R.string.app_name);
                                mFragment = MainFragment.newInstance(drawerItem.getType());
                                break;
                        }

                       /* if (mFragment != null){
                            FragmentTransaction ft = mFragmentManager.beginTransaction();
                            ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                            ft.replace(R.id.drawer_layout, mFragment);
                            ft.commit();
                            getSupportActionBar().setTitle(title);
                        }*/

                        if (mFragment != null) {
                            mFragmentManager.beginTransaction().replace(R.id.drawer_layout, mFragment).commit();
                        }

                        getSupportActionBar().setTitle(title);

                        result.closeDrawer();
                        return true;
                    }
                })
                .build();

        result.setSelection(item1);

        if (mFragment != null){
            FragmentTransaction ft = mFragmentManager.beginTransaction();
            ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            ft.replace(R.id.drawer_layout, mFragment);
            ft.commit();
            getSupportActionBar().setTitle(title);
        }


        toggle = new ActionBarDrawerToggle(this, result.getDrawerLayout(), R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mediaScreen.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);

        mAdViewPlayer = (AdView) findViewById(R.id.ads_player_live);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("XT1032").build();
        mAdViewPlayer.loadAd(adRequest);

    }


    @Override
    public void onDrawerOpened(View drawerView) {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onDrawerClosed(View drawerView) {

    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        frameAnimation = (AnimationDrawable) animate.getBackground();
        isShowHiddePannel();
        registerReceiver(trackingServiceRadio, new IntentFilter(APlayerService.RADIO_STATUS_START));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        this.menu = menu;

        menItemMode = menu.findItem(R.id.mode);

        if(preferences.getValueBoolean(getString(R.string.mode_list))){
            menItemMode.setIcon(getResources().getDrawable(R.drawable.ic_grid));
        }else{
            menItemMode.setIcon(getResources().getDrawable(R.drawable.ic_linear));
        }

        SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();


        SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextChange(String newText){

                if(newText.length()==0){
                    if(modeFragmentType==0){
                        ViewPagerFragment vf = (ViewPagerFragment) mFragment;
                        vf.restoreRadioLocales(modeList);
                    }
                }

                return true;
            }
            @Override
            public boolean onQueryTextSubmit(String query){

                if(modeFragmentType==0){
                    ViewPagerFragment vf = (ViewPagerFragment) mFragment;
                    vf.searchRadioLocales(model.listRadioBySearch(query.trim()), modeList);
                }

                //Toast.makeText(MainActivity.this, "on query submit: "+query, Toast.LENGTH_LONG).show();
                return true;
            }

        };
        searchView.setOnQueryTextListener(textChangeListener);
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                //Toast.makeText(MainActivity.this, "close search", Toast.LENGTH_LONG).show();
                if(modeFragmentType==0){
                    ViewPagerFragment vf = (ViewPagerFragment) mFragment;
                    vf.restoreRadioLocales(modeList);
                }
                return false;
            }
        });


        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mode:
                modeList = !modeList;
                preferences.saveValueBoolean(getString(R.string.mode_list),modeList);
                if(modeList){
                    item.setIcon(getResources().getDrawable(R.drawable.ic_grid));
                }else{
                    item.setIcon(getResources().getDrawable(R.drawable.ic_linear));
                }

                if(modeFragmentType==0){
                    ViewPagerFragment vf = (ViewPagerFragment) mFragment;
                    vf.refreshModeFragment(modeList);
                }

                if(modeFragmentType==1){
                    FragmentFavoritos vf = (FragmentFavoritos) mFragment;
                    vf.changeModeRecycler(modeList);
                }

                if(modeFragmentType==2){
                    FragmentRecientes vf = (FragmentRecientes) mFragment;
                    vf.changeModeRecycler(modeList);
                }


                return true;
            default:
                return super.onOptionsItemSelected(item);
        }


    }

    @Override
    public void onSelectedListerGenero(Extra extra, View v) {


        Bundle args = new Bundle();
        args.putSerializable(getString(R.string.entity_category), extra);


            Intent intent = new Intent(this, RadiosCategory.class);
            intent.putExtras(args);
            startActivity(intent);


    }

    private void stopRadio(){
        model.closePlayingRadio();
        stopService(acpServices);
        hiddeCollapsePlayer();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("onDestroy","MainActivity");
        stopRadio();
        unregisterReceiver(trackingServiceRadio);
    }

    private void likeRadio(){
        if(!PlayerConstants.RADIO_LIKE){
            PlayerConstants.RADIO_LIKE=true;
            model.likeRadio(currentRadio.getId(),1);
            Utils.animateLike(btnLike);
        }else{
            PlayerConstants.RADIO_LIKE=false;
            model.likeRadio(currentRadio.getId(), 0);
            Utils.animateUnLike(btnLike);
        }

        vf = (ViewPagerFragment) mFragment;
        vf.refreshFavoritesFragment();
        PlayerConstants.RADIO_LIKE_HANDLER.sendMessage(PlayerConstants.RADIO_LIKE_HANDLER.obtainMessage());
    }

    private void loadInfoRadio(){
        currentRadio = model.getCurrenPlayingRadio();


        if(currentRadio!=null) {
            radioName.setText(currentRadio.getName());
            if(currentRadio.getFavorito()==0) {
                PlayerConstants.RADIO_LIKE = false;
                btnLike.setImageResource(R.drawable.ic_like_off);
            }else {
                PlayerConstants.RADIO_LIKE = true;
                btnLike.setImageResource(R.drawable.ic_like_on);
            }

            Picasso.with(this)
                    .load(currentRadio.getImage())
                    .resize(Utils.dpToPx(this,250), Utils.dpToPx(this,250))
                    .centerCrop()
                    .into(radioImage);

            Picasso.with(this)
                    .load(currentRadio.getImage())
                    .resize(Utils.dpToPx(this,35), Utils.dpToPx(this,35))
                    .centerCrop()
                    .into(radioIcon);

        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.stop:
                stopRadio();
                break;
            case R.id.like:
                likeRadio();
                break;
        }

    }



    @Override
    public void onBackPressed() {

        if(mediaScreen.getPanelState().equals(SlidingUpPanelLayout.PanelState.EXPANDED)){
            Log.e("SHOW COLLAPSE BACKPRESS", "ok");
            mediaScreen.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        }else{
            super.onBackPressed();
        }

    }


    private BroadcastReceiver trackingServiceRadio = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub

            int status = intent.getExtras().getInt(getString(R.string.radio_status));

            switch(status){
                case 0:
                    isShowHiddePannel();
                    break;
                case -1:
                    Toast.makeText(MainActivity.this, getString(R.string.msg_error_server),Toast.LENGTH_SHORT).show();
                    stopRadio();
                    break;
                case 1:
                    showCollapsePlayer();
                    break;
                case 2:
                    hiddeCollapsePlayer();
                    break;
            }
        }
    };



    private void isShowHiddePannel(){
        if(model.isSelected() && model.getStatusCurrenRadioSelected(model.getCurrenPlayingRadio().getId())==1)
            showCollapsePlayer();
        else
            hiddeCollapsePlayer();
    }

    private void showCollapsePlayer(){
        loadInfoRadio();
        mediaScreen.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        container.setPadding(0, 0, 0, padding_70dp);
        frameAnimation.start();
    }

    private void hiddeCollapsePlayer(){
        mediaScreen.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        container.setPadding(0, 0, 0, 0);
        frameAnimation.stop();
    }

    public static void changeLikeNotification() {
        if(PlayerConstants.RADIO_LIKE){
            Utils.animateLike(btnLike);
        }else{
            Utils.animateUnLike(btnLike);
        }
    }



}
