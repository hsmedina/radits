package com.stbig.radits;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Window;
import android.widget.TextView;

import com.stbig.radits.api.ApiExtra;
import com.stbig.radits.api.ApiRadios;
import com.stbig.radits.datos.AppPreferences;
import com.stbig.radits.helper.imagen.Utils;

import java.util.Locale;


/**
 * Created by root on 17/11/15.
 */
public class SplashActivity extends Activity {

    private TextView name;
    private AppPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);

        name = (TextView) findViewById(R.id.name_app);
        Typeface t = Typeface.createFromAsset(getAssets(), "Shihan.otf");
        name.setTypeface(t);
        preferences = new AppPreferences(this);

    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.i("locale ", Locale.getDefault().toString().replace("_", "-"));
        Log.i("iso country ", Locale.getDefault().getISO3Country());

        if(preferences.getValue(getString(R.string.country_iso)).equals(Locale.getDefault().getISO3Country())){
            startApp();
        }else{
            preferences.saveValue(getString(R.string.country_iso), Locale.getDefault().getISO3Country());
            if(Utils.isConnectingToInternet(this))
                loadData(Locale.getDefault().toString().replace("_", "-"), "PER");
        }

    }

    private void loadData(final String parm2,final String parm3){
        new Thread(new Runnable() {
            @Override
            public void run() {
                ApiRadios api = new ApiRadios(SplashActivity.this);
                boolean response = api.requestRadios("local",parm2,parm3);

                ApiExtra pa = new ApiExtra(SplashActivity.this);
                boolean response_pa = pa.requestExtras( "country",parm2);

                ApiExtra mu = new ApiExtra(SplashActivity.this);
                boolean response_mu = mu.requestExtras("music",parm2);

                ApiExtra sp = new ApiExtra(SplashActivity.this);
                boolean response_sp = sp.requestExtras("sports",parm2);

                ApiExtra no = new ApiExtra(SplashActivity.this);
                boolean response_no = no.requestExtras("talk",parm2);


                Message msg = new Message();
                msg.obj=response_no;
                responseWeb.sendMessage(msg);

            }
        }).start();


    }

    private Handler responseWeb = new Handler(){
        public void handleMessage(Message msg){

            if(Boolean.valueOf(msg.obj.toString())){
                startApp();
            }
    };
    };

    private void startApp(){
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
    }



}
