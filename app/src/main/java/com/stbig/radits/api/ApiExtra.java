package com.stbig.radits.api;

import android.content.Context;
import android.util.Log;

import com.stbig.radits.R;
import com.stbig.radits.datos.Datos;
import com.stbig.radits.helper.DBitem;
import com.stbig.radits.http.HttpConnectionWeb;

import org.json.JSONArray;
import org.json.JSONObject;


/**
 * Created by root on 13/01/16.
 */
public class ApiExtra {
    private Context context;
    private HttpConnectionWeb cn;
    private Datos data;

    public ApiExtra(Context context){
        this.context=context;
        cn = new HttpConnectionWeb(context.getString(R.string.radits_api));
        data = new Datos(context);
    }


    public boolean requestExtras(String pamr1,String pamr2){

        cn.AddParam(context.getString(R.string.parm1), pamr1);
        cn.AddParam(context.getString(R.string.parm2), pamr2);

        try {

            String json = cn.connect();

            if(!json.isEmpty()){
                JSONArray info = new JSONArray(json);
                    if(registerExtra(info))
                        return true;

            }

            return false;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
    }

    public boolean registerExtra(JSONArray extras){

        try{
            for(int x=0;x<extras.length() -1 ;x++){

                JSONObject node = extras.getJSONObject(x);

                data.registerExtras(node.getString(DBitem.EXTRA_ID),
                        node.getString(DBitem.EXTRA_LOCALE),
                        node.getString(DBitem.EXTRA_NOMBRE),
                        node.getString(DBitem.EXTRA_TYPE),
                        node.getString(DBitem.EXTRA_TUN),
                        node.getString(DBitem.EXTRA_TUN_PARENT),
                        node.getString(DBitem.EXTRA_ISO));

               // Log.i("se cae en", " res " + String.valueOf(x));

            }

            return true;
        }catch(Exception e){
            Log.e("error media", e.getMessage());
            return false;
        }

    }



    public boolean requestRadiosByCategory(String pamr1,String pamr2){

        cn.AddParam(context.getString(R.string.parm1), pamr1);
        cn.AddParam(context.getString(R.string.parm4), pamr2);

        try {

            String json = cn.connect();

            if(!json.isEmpty()){

                JSONArray info = new JSONArray(json);

                if(registerRadio(info, pamr2))
                    return true;

            }

            return false;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
    }


    public boolean requestRadiosByCountry(String key, String locale, String iso, String id_categopry){

        cn.AddParam(context.getString(R.string.parm1), key);
        cn.AddParam(context.getString(R.string.parm2), locale);
        cn.AddParam(context.getString(R.string.parm3), iso);

        try {

            String json = cn.connect();

            if(!json.isEmpty()){
                JSONArray info = new JSONArray(json);
                if(registerRadio(info,id_categopry))
                    return true;

            }

            return false;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }

    }


    public boolean registerRadio(JSONArray radios, String category){

        try{

            for(int x=0;x<radios.length() -1 ;x++){

                JSONObject node = radios.getJSONObject(x);

                data.registerRadio(node.getString(DBitem.RADIO_ID),
                        node.getString(DBitem.RADIO_CATEGORY),
                        node.getString(DBitem.RADIO_NOMBRE),
                        node.getString(DBitem.RADIO_IMAGE),
                        node.getString(DBitem.RADIO_URL),
                        node.getString(DBitem.RADIO_ISO),
                        node.getString(DBitem.RADIO_GENERO),
                        node.getString(DBitem.RADIO_STATUS),
                        node.getString(DBitem.RADIO_TUNEIS_ID),
                        category,
                        0, "" , 0,0,0);

            }

            return true;
        }catch(Exception e){

            Log.i("error radios ", e.getMessage());
            return false;
        }

    }




}
