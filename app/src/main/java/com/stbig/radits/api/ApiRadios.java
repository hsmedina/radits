package com.stbig.radits.api;

import android.content.Context;
import android.util.Log;

import com.stbig.radits.R;
import com.stbig.radits.datos.Datos;
import com.stbig.radits.helper.DBitem;
import com.stbig.radits.helper.Files;
import com.stbig.radits.http.HttpConnectionWeb;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

/**
 * Created by root on 23/11/15.
 */
public class ApiRadios {
    private Context context;
    private HttpConnectionWeb cn;
    private Datos data;

    public ApiRadios(Context context){
        this.context=context;
        cn = new HttpConnectionWeb(context.getString(R.string.radits_api));
        data = new Datos(context);
    }

    public ApiRadios(Context context, String url){
        this.context=context;
        cn = new HttpConnectionWeb(url);
        data = new Datos(context);
    }


    public boolean requestRadios(String pamr1, String parm2, String parm3){

        cn.AddParam(context.getString(R.string.parm1), pamr1);
        cn.AddParam(context.getString(R.string.parm2), parm2);
        cn.AddParam(context.getString(R.string.parm3), parm3);

        try {

            String json = cn.connect();

            if(!json.isEmpty()){
                JSONArray info = new JSONArray(json);
                if(registerRadio(info))
                    return true;

            }

            return false;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }

    }


    public boolean requestRadiosPopulares(String iso_lang, String iso_country){

        cn.AddParam(context.getString(R.string.parm1), "ranking");
        cn.AddParam(context.getString(R.string.parm2), iso_lang);
        cn.AddParam(context.getString(R.string.parm3), iso_country);

        try {

            String json = cn.connect();

            if(!json.isEmpty()){

                JSONArray info = new JSONArray(json);
                if(registerViewRadio(info))
                    return true;

            }

            return false;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }

    }

    public boolean registerRadio(JSONArray radios){

        try{

            // Files nomediaFile = new Files();
            //  nomediaFile.setNameFiles(".nomedia");
            //  File f = new File(nomediaFile.getPathService());
            //  f.createNewFile();

            for(int x=0;x<radios.length() -1 ;x++){

                JSONObject node = radios.getJSONObject(x);

                data.registerRadio(node.getString(DBitem.RADIO_ID),
                        node.getString(DBitem.RADIO_CATEGORY),
                        node.getString(DBitem.RADIO_NOMBRE).trim(),
                        node.getString(DBitem.RADIO_IMAGE),
                        node.getString(DBitem.RADIO_URL),
                        node.getString(DBitem.RADIO_ISO),
                        node.getString(DBitem.RADIO_GENERO),
                        node.getString(DBitem.RADIO_STATUS),
                        node.getString(DBitem.RADIO_TUNEIS_ID),
                        "",
                        0, "", 0, 0,0);

                //cn.downloadFile(node.getString(DBitem.RADIO_IMAGE));

            }

            return true;
        }catch(Exception e){

            Log.i("error radios ",e.getMessage());
            return false;
        }
    }


    public boolean registerViewRadio(JSONArray radios){

        try{


            for(int x=0;x<radios.length() -1 ;x++){

                JSONObject node = radios.getJSONObject(x);

                data.registerViewsRadio(node.getString(DBitem.RADIO_ID),
                        Integer.parseInt(node.getString(DBitem.RADIO_VIEW).trim()));

            }

            return true;
        }catch(Exception e){

            Log.i("error radios ",e.getMessage());
            return false;
        }

    }


    public boolean registerVisitRadio(String id_radio,String iso_country){

        cn.AddParam("id_radio", id_radio);
        cn.AddParam(context.getString(R.string.flag_type), "ranking");
        cn.AddParam(context.getString(R.string.parm3), iso_country);

        try {
            String json = cn.connect();
            if(!json.isEmpty())
                    return true;


            return false;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e("error reg radios ",e.getMessage());
            e.printStackTrace();
            return false;
        }

    }




}
