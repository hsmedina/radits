package com.stbig.radits.helper;

import android.content.Context;
import android.util.Log;

import com.stbig.radits.datos.Datos;
import com.stbig.radits.objects.Radio;

import java.util.ArrayList;

/**
 * Created by root on 17/03/16.
 */
public class ManagerModel {

    private Context context;
    private Datos data;

    public ManagerModel(Context context){
        this.context=context;
        data = new Datos(context);
    }

    public boolean isSelected(){
        if(data.getRadioSelected()==null)
            return false;
        else
            return true;

    }

    public int getStatusCurrenRadioSelected(String id){
            return data.getStatusPlayerRadio(id).getIsplaying();
    }

    public void closePlayingRadio(){
        data.cleanPlayingRadio();
    }

    public void  likeRadio(String id,int value){
        Log.i("like Radio id", String.valueOf(id));
        Log.i("like Radio value", String.valueOf(value));
        data.setLikeRadio(id, value);
    }

    public Radio getCurrenPlayingRadio(){
        return data.getRadioSelected();
    }

    public ArrayList<Radio> listRadioCategory(String category){
        return data.listarRadiosByCategory(category);
    }

    public ArrayList<Radio> listRadioCategorySearch(String category,String query){
        return data.listarRadiosByCategorySearch(category, query);
    }

    public ArrayList<Radio> listRadioPopulares(){
        return data.listarPopulares();
    }

    public ArrayList<Radio> listRadioPaises(String iso){
        return data.listarRadiosByPais(iso);
    }

    public ArrayList<Radio> listRadioPaisesSearch(String iso, String query){
        return data.listarRadiosByPaisSearch(iso, query);
    }


    public ArrayList<Radio> listRadioBySearch(String query){
        return data.listarRadiosBySearch(query);
    }
}
