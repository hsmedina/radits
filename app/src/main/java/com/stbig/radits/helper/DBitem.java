package com.stbig.radits.helper;

import android.provider.BaseColumns;

public class DBitem implements BaseColumns {
	

	public static final String TABLE_RADIO = "RADIOS";
	public static final String RADIO_ID = "id";
	public static final String RADIO_CATEGORY = "category_id";
	public static final String RADIO_NOMBRE = "radio_name";
	public static final String RADIO_IMAGE = "radio_image";
	public static final String RADIO_URL = "radio_url";
	public static final String RADIO_ISO = "iso";
	public static final String RADIO_GENERO = "genre_id";
	public static final String RADIO_STATUS = "radio_status";
	public static final String RADIO_TUNEIS_ID = "tunein_id";
	public static final String RADIO_CATEGORI_FILL = "category_fill";
	public static final String RADIO_FAVORITO = "favorito";
	public static final String RADIO_RECIENTE = "reciente";
	public static final String RADIO_PLAYING = "playing";
	public static final String RADIO_VIEW = "views";
	public static final String RADIO_SELECTED = "selected";


	public static final String TABLE_EXTRA = "EXTRA";
	public static final String EXTRA_ID = "id";
	public static final String EXTRA_LOCALE = "locale";
	public static final String EXTRA_NOMBRE = "name_aux";
	public static final String EXTRA_TYPE = "type_aux";
	public static final String EXTRA_TUN = "id_tun";
	public static final String EXTRA_TUN_PARENT = "id_tun_parent";
	public static final String EXTRA_ISO = "iso";


}
