package com.stbig.radits.helper;

/**
 * Created by root on 04/02/16.
 */
public enum AlphabetGroup {
    A ("A","A"),
    B ("B","B"),
    C ("C","C"),
    D ("D","D"),
    E ("E","E"),
    F ("F","F"),
    G ("G","G"),
    H ("H","H"),
    I ("I","I"),
    J ("J","J"),
    K ("K","K"),
    L ("L","L"),
    M ("M","M"),
    N ("N","N"),
    O ("O","O"),
    P ("P","P"),
    Q ("Q","Q"),
    R ("R","R"),
    S ("S","S"),
    T ("T","T"),
    U ("U","U"),
    V ("V","V"),
    W ("W","W"),
    X ("X","X"),
    Y ("Y","Y"),
    Z ("Z","Z"),;


    private final String value;
    private final String mName;

    AlphabetGroup (String value, String nameGroup){
        this.value=value;
        this.mName=nameGroup;
    }

    public String getName() {
        return mName;
    }

    public boolean containsHue(String itemValue) {
        return (itemValue.toString().equals(value));
    }

}
