package com.stbig.radits.helper;

import android.support.annotation.NonNull;

/**
 * Created by root on 04/02/16.
 */
public class AlphabetGroupCalculator {


    public AlphabetGroup getAlphaGroup(String letter) {
        for (AlphabetGroup group : AlphabetGroup.values()) {
            if (group.containsHue(letter)) {
                return group;
            }
        }

        throw new NullPointerException("Could not classify hue into Color Group!");
    }
}
