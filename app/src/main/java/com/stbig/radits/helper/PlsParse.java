package com.stbig.radits.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by root on 19/02/16.
 */
public class PlsParse  {

    private final BufferedReader reader;

    public PlsParse(String url) throws MalformedURLException, IOException {
        URLConnection urlConnection = new URL(url).openConnection();
        this.reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
    }

    public List<String> getUrls() {
        LinkedList<String> urls = new LinkedList<String>();
        while (true) {
            try {
                String line = reader.readLine();
                android.util.Log.w("pls", "-"+line);
                if (line == null) {
                    break;
                }
                String url = parseLine(line);
                if (url != null && !url.equals("")) {
                    urls.add(url);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return urls;
    }

    private String parseLine(String line) {
        if (line == null) {
            return null;
        }
        String trimmed = line.trim();
        if (trimmed.indexOf("http") >= 0) {
            return trimmed.substring(trimmed.indexOf("http"));
        }
        return "";
    }
}