package com.stbig.radits.helper;

import android.os.Handler;

import java.util.ArrayList;

public class PlayerConstants {

	//radio is playing or stoped
	public static boolean RADIO_STOP = false;
	//radio is recording or not
	public static boolean RADIO_RECORD = false;
	//radio is recording or not
	public static boolean RADIO_LIKE = false;
	//radio is error or not
	public static boolean RADIO_ERROR = false;

	//handler for radio like
	public static Handler RADIO_LIKE_HANDLER;
	//handler for radio play/stoped
	public static Handler PLAY_STOP_HANDLER;

	public static Handler PLAY_STOP_PHONE_CALL_STATE;

}
