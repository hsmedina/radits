package com.stbig.radits.adapters;

import android.content.Context;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import android.widget.TextView;

import com.stbig.radits.R;

import com.stbig.radits.helper.imagen.Utils;
import com.stbig.radits.objects.Extra;


import java.util.ArrayList;

/**
 * Created by root on 19/01/16.
 */
public class AdapterExtra  extends RecyclerView.Adapter<AdapterExtra.ViewHolder>{

    private Context context;
    private ArrayList<Extra> extras;
    private int icon_news;
    private int icon_music;
    private int icon_deportes;
    private int icon_pais;


    public AdapterExtra(Context context, ArrayList<Extra> extras){
        this.context=context;
        this.extras=extras;
        icon_news = R.drawable.ic_item_news;
        icon_music = R.drawable.ic_item_music;
        icon_deportes = R.drawable.ic_item_deportes;
        icon_pais = R.drawable.ic_item_country;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getItemCount() {
        return extras.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.nombre.setText(extras.get(position).getNombre());
        Utils.setFontTexV(context,holder.nombre);

        if(extras.get(position).getType().toString().trim().equals("music"))
            holder.image.setBackgroundDrawable(context.getResources().getDrawable(icon_music));

        if(extras.get(position).getType().toString().trim().equals("talk"))
            holder.image.setBackgroundDrawable(context.getResources().getDrawable(icon_news));

        if(extras.get(position).getType().toString().trim().equals("sports"))
            holder.image.setBackgroundDrawable(context.getResources().getDrawable(icon_deportes));

        if(extras.get(position).getType().toString().trim().equals("country"))
            holder.image.setBackgroundDrawable(context.getResources().getDrawable(icon_pais));


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_extra, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    public  static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView nombre;
        ImageView image;

        public ViewHolder(View itemView){
            super(itemView);

            nombre = (TextView) itemView.findViewById(R.id.item_name);
            image = (ImageView) itemView.findViewById(R.id.item_icon);

        }

        @Override
        public void onClick(View v) {

        }


    }


}
