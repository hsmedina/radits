package com.stbig.radits.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.andraskindler.quickscroll.Scrollable;
import com.stbig.radits.R;
import com.stbig.radits.helper.AlphabetGroup;
import com.stbig.radits.helper.imagen.Utils;
import com.stbig.radits.objects.Extra;
import com.stbig.radits.objects.Pais;

import java.util.ArrayList;

/**
 * Created by root on 04/02/16.
 */
public class AdapterPais   extends RecyclerView.Adapter<AdapterPais.ViewHolder> {

    private Context context;
    private ArrayList<Pais> paises;
    private int icon_pais;
    private boolean mode;



    public AdapterPais(Context context, ArrayList<Pais> paises, boolean mode){
        this.context=context;
        this.paises=paises;
        icon_pais = R.drawable.ic_item_country;
        this.mode = mode;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getItemCount() {
        return paises.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.nombre.setText(paises.get(position).getNombre());
        holder.image.setBackgroundDrawable(context.getResources().getDrawable(icon_pais));
        Utils.setFontTexV(context, holder.nombre);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        if(mode)
             v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_pais, parent, false);
        else
             v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_pais, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    public  static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView nombre;
        ImageView image;

        public ViewHolder(View itemView){
            super(itemView);

            nombre = (TextView) itemView.findViewById(R.id.item_name);
            image = (ImageView) itemView.findViewById(R.id.item_icon);

        }

        @Override
        public void onClick(View v) {

        }


    }

}
