package com.stbig.radits.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.RippleDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.stbig.radits.R;
import com.stbig.radits.helper.imagen.CircleTransform;
import com.stbig.radits.helper.imagen.Utils;
import com.stbig.radits.objects.Radio;

import java.util.ArrayList;

/**
 * Created by root on 08/12/15.
 */
public class AdapterRadios extends RecyclerView.Adapter<AdapterRadios.ViewHolder> {

    private Context context;
    private ArrayList<Radio> radios;
    private boolean mode;
    private boolean popular=false;


    public AdapterRadios(Context context, ArrayList<Radio> radios, boolean mode){
        this.context=context;
        this.radios=radios;
        this.mode = mode;
    }

    public AdapterRadios(Context context, ArrayList<Radio> radios, boolean mode, boolean popular){
        this.context=context;
        this.radios=radios;
        this.mode = mode;
        this.popular=popular;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getItemCount() {
        return radios.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.nombre.setText(radios.get(position).getName());
        Utils.setFontTexV(context, holder.nombre);
        Utils.setFontTexV(context, holder.numView);

        if(!mode) {
            Picasso.with(context)
                    .load(radios.get(position).getImage())
                    .resize(Utils.dpToPx(context, 100), Utils.dpToPx(context, 100))
                    .centerCrop()
                    .into(holder.image);
        }else{
            Picasso.with(context)
                    .load(radios.get(position).getImage())
                    .resize(Utils.dpToPx(context, 50), Utils.dpToPx(context, 50))
                    .centerCrop()
                    .transform(new CircleTransform())
                    .into(holder.image);
        }


        if(popular)
            holder.contentViews.setVisibility(View.VISIBLE);
        else
            holder.contentViews.setVisibility(View.GONE);


        holder.numView.setText(String.valueOf(radios.get(position).getView()) + "  " + context.getString(R.string.txt_reproduccion));




    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;

        if(!mode)
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_grid_radios, null, false);
        else
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_linear_radios, parent, false);


        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    public  static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView nombre;
        ImageView image;
        LinearLayout contentViews;
        TextView numView;

        public ViewHolder(View itemView){
            super(itemView);

            nombre = (TextView) itemView.findViewById(R.id.radio_name);
            image = (ImageView) itemView.findViewById(R.id.radio_image);
            contentViews = (LinearLayout) itemView.findViewById(R.id.contetViews);
            numView = (TextView) itemView.findViewById(R.id.numViews);

        }

        @Override
        public void onClick(View v) {

        }
    }
}
