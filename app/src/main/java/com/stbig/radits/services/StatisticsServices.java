package com.stbig.radits.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.stbig.radits.R;
import com.stbig.radits.api.ApiRadios;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by hsmedina on 6/17/2016.
 */
public class StatisticsServices extends IntentService {

    private ApiRadios api;
    private String radio_id;
    private Context context;

    public StatisticsServices(){
        super("StatisticsServices");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        api = new ApiRadios(context, getString(R.string.radits_api_statistics));
    }

    @Override
    public IBinder onBind(Intent intent) {
        return super.onBind(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle values = intent.getExtras();

        if(values!=null){
            radio_id = values.getString("radio_id");
            api.registerVisitRadio(radio_id, "PER");
        }
    }
}
