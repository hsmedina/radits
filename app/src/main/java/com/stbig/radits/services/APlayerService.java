package com.stbig.radits.services;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.RemoteControlClient;
import android.os.Bundle;

import android.os.Handler;
import android.os.IBinder;

import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;


import com.stbig.radits.R;
import com.stbig.radits.ReproductorActivity;
import com.stbig.radits.datos.AppPreferences;

import com.stbig.radits.datos.Datos;
import com.stbig.radits.helper.PlayerConstants;
import com.stbig.radits.helper.PlsParse;
import com.stbig.radits.helper.imagen.Utils;

import com.spoledge.aacdecoder.MultiPlayer;
import com.spoledge.aacdecoder.PlayerCallback;
import com.stbig.radits.objects.Radio;
import com.stbig.radits.receiver.NotificationBroadcast;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by root on 28/01/16.
 */
public class APlayerService extends  Service implements AudioManager.OnAudioFocusChangeListener {

    private Context context;
    private static Bundle extra;
    private AppPreferences preferences;
    private static Radio radioSeleted;
    private Datos data;
    private Intent radioStatus;
    private Intent radioVisit;

    private ComponentName remoteComponentName;
    private RemoteControlClient remoteControlClient;
    AudioManager audioManager;

    // AAC Service
    private MultiPlayer aacMp3Player=null;
    private PlayerCallback clb;

    // Media Player Service
    private MediaPlayer mPlayer;

    public static final String RADIO_STATUS_START="com.stbig.radits.services.APlayerService";


    // Media Player Service notification
    int NOTIFICATION_ID = 1111;
    public static final String NOTIFY_CLOSE = "com.stbig.radits.audioplayer.close";
    public static final String NOTIFY_STOP = "com.stbig.radits.audioplayer.stop";
    public static final String NOTIFY_PLAY = "com.stbig.radits.audioplayer.play";
    public static final String NOTIFY_LIKE = "com.stbig.radits.audioplayer.like";
    private static boolean currentVersionSupportBigNotification = false;
    private static boolean currentVersionSupportLockScreenControls = false;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        preferences = new AppPreferences(this);
        data = new Datos(this);
        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);

        Log.i("onCreate","Services player");
        currentVersionSupportBigNotification = Utils.currentVersionSupportBigNotification();
        currentVersionSupportLockScreenControls = Utils.currentVersionSupportLockScreenControls();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.i("onStartCommand","Services player");

        radioStatus = new Intent(RADIO_STATUS_START);
        radioVisit =  new Intent(this, StatisticsServices.class);

        extra = intent.getExtras();

        if(extra!=null){

           radioSeleted = (Radio) extra.get(getString(R.string.entity_radio));
           data.setRecienteRadio(radioSeleted.getId(), Utils.getDateTime());

            Log.e("update selected", radioSeleted.getId() + " selected");
           data.setSelectedRadio(radioSeleted.getId(),1);
           if(currentVersionSupportLockScreenControls){
               RegisterRemoteClient();
           }

          new Thread(new Runnable() {
              @Override
              public void run() {
                  UpdateMetadata(radioSeleted);
              }
          }).start();

           playRadio();

        }


        PlayerConstants.RADIO_LIKE_HANDLER = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                Log.e("start services", "handler like");
                newNotification();
                return false;
            }
        });

        PlayerConstants.PLAY_STOP_HANDLER = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                String message = (String)msg.obj;

                if(message.equalsIgnoreCase(getResources().getString(R.string.play))){
                    PlayerConstants.RADIO_STOP = false;
                    if(currentVersionSupportLockScreenControls){
                        remoteControlClient.setPlaybackState(RemoteControlClient.PLAYSTATE_PLAYING);
                    }

                    playRadio();
                }else if(message.equalsIgnoreCase(getResources().getString(R.string.stop))){
                    PlayerConstants.RADIO_STOP = true;
                    if(currentVersionSupportLockScreenControls){
                        remoteControlClient.setPlaybackState(RemoteControlClient.PLAYSTATE_PAUSED);
                    }
                    stopRadio();
                }

                //ReproductorActivity.changeStopPlayNotification();
                newNotification();

                Log.d("TAG", "TAG Pressed: " + message);
                return false;
            }
        });

        return START_NOT_STICKY;
    }



    private void playRadio(){
        if(radioSeleted!=null){
            if(radioSeleted.getUrl().toLowerCase().contains(".pls")){
                parsePlsUrl(radioSeleted.getUrl());
            }else{
                selectMethodPlay(radioSeleted.getUrl());
            }
        }

    }


    private void onPlayAAC(final String idRadio,final String source){
        new Thread(new Runnable() {
            @Override
            public void run() {

                clb = new PlayerCallback() {
                    @Override
                    public void playerStarted() {
                        Log.i("service playerStarted", "  si ");
                        data.setStatusPlayerRadio(idRadio, 1);
                        responseSuccessPlay();
                        registerVisit();
                        newNotification();
                    }

                    @Override
                    public void playerPCMFeedBuffer(boolean b, int i, int i1) {
                        Log.i("service PCMFeedBuffer", "  si ");
                    }

                    @Override
                    public void playerStopped(int i) {
                        Log.i("service playerStopped", "  si ");
                    }

                    @Override
                    public void playerException(Throwable throwable) {
                        Log.i("error service AAC", throwable.getMessage());
                        //onPlayMedia(radioSeleted.getId(),radioSeleted.getUrl());
                        responseErrorPlay();
                    }

                    @Override
                    public void playerMetadata(String s, String s1) {
                        Log.i("service playerMetadata", "  si ");
                    }

                    @Override
                    public void playerAudioTrackCreated(AudioTrack audioTrack) {
                        Log.i("service TrackCreated", "si");
                    }
                };

                aacMp3Player = new MultiPlayer(clb);
                aacMp3Player.playAsync(source);

            }
        }).start();
    }

    private void parsePlsUrl(final String url){

        new Thread(new Runnable() {
            @Override
            public void run() {

                try{
                    PlsParse parse = new PlsParse(url);
                    List<String> lista = parse.getUrls();
                    Log.i("parse url 0", lista.get(0));
                    selectMethodPlay(lista.get(0));
                }catch(IOException e){
                    Log.e("error service custom", e.getMessage());
                }

            }
        }).start();
    }

    private void selectMethodPlay(String source){
        if (source.contains("rtsp")){
            onPlayMedia(radioSeleted.getId(),radioSeleted.getUrl());
        }else{
            onPlayAAC(radioSeleted.getId(), radioSeleted.getUrl());
        }
    }


    private void stopAAC(){
        if(aacMp3Player!=null) {
            aacMp3Player.stop();
            aacMp3Player = null;
        }
    }


    private void onPlayMedia(final String idRadio, final String source){

        new Thread(new Runnable() {
            @Override
            public void run() {

                try{

                    mPlayer = new MediaPlayer();
                    mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mPlayer.setDataSource(source);
                    mPlayer.prepareAsync();
                    mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                            data.setStatusPlayerRadio(idRadio, 1);
                            responseSuccessPlay();
                            registerVisit();
                            newNotification();
                           // preferences.saveValueInt(getString(R.string.radio_status), 1);
                        }
                    });

                    mPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                        @Override
                        public boolean onError(MediaPlayer mp, int what, int extra) {
                            Log.e("error service media ","error en media player");
                            responseErrorPlay();
                            return false;
                        }
                    });


                }catch (Exception e){
                    Log.e("error service media ",e.getMessage());
                    responseErrorPlay();
                }



            }
        }).start();

    }

    private void stopMedia(){
        if(mPlayer != null) {
            mPlayer.stop();
            mPlayer.release();
            mPlayer = null;
        }
    }

    private void stopRadio(){
        stopAAC();
        stopMedia();
        responseStopPlayer();
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("onDestroy","Services player");
        stopAAC();
        stopMedia();
        responseClosed();
    }

    private void responseClosed(){
        radioStatus.putExtra(context.getString(R.string.radio_status), 0);
        context.sendBroadcast(radioStatus);
        if(radioSeleted!=null) {
            Log.e("update Unselected", radioSeleted.getId() + " unselected");
            data.setSelectedRadio(radioSeleted.getId(), 0);
        }
    }

    private void responseErrorPlay(){
        radioStatus.putExtra(context.getString(R.string.radio_status), -1);
        context.sendBroadcast(radioStatus);
        if(radioSeleted!=null)
            data.setStatusPlayerRadio(radioSeleted.getId(), 0);
    }

    private void responseSuccessPlay(){
        radioStatus.putExtra(context.getString(R.string.radio_status), 1);
        context.sendBroadcast(radioStatus);

        if(radioSeleted!=null)
            data.setStatusPlayerRadio(radioSeleted.getId(), 1);
    }

    private void responseStopPlayer(){
        radioStatus.putExtra(context.getString(R.string.radio_status), 2);
        context.sendBroadcast(radioStatus);

        if(radioSeleted!=null)
            data.setStatusPlayerRadio(radioSeleted.getId(), 0);
    }

    private void registerVisit(){
        radioVisit.putExtra("radio_id",radioSeleted.getId());
        startService(radioVisit);
    }



    @SuppressLint("NewApi")
    private void newNotification() {
        String songName = radioSeleted.getName();
        String albumName = getString(R.string.subtitle_cancion);
        RemoteViews simpleContentView = new RemoteViews(getApplicationContext().getPackageName(),R.layout.fragment_notification_media);
        RemoteViews expandedView = new RemoteViews(getApplicationContext().getPackageName(), R.layout.fragment_notification_media_big);

        Notification notification = new NotificationCompat.Builder(getApplicationContext())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(songName).build();

        setListeners(simpleContentView);
        setListeners(expandedView);

        notification.contentView = simpleContentView;
        if(currentVersionSupportBigNotification){
            notification.bigContentView = expandedView;
        }

        try{
            //long albumId = PlayerConstants.SONGS_LIST.get(PlayerConstants.SONG_NUMBER).getAlbumId();
            //Bitmap albumArt = UtilFunctions.getAlbumart(getApplicationContext(), albumId);
            Bitmap albumArt = getBitmapFromURL(radioSeleted.getImage());
            if(albumArt != null){
                notification.contentView.setImageViewBitmap(R.id.imageViewAlbumArt, albumArt);
                if(currentVersionSupportBigNotification){
                    notification.bigContentView.setImageViewBitmap(R.id.imageViewAlbumArt, albumArt);
                }
            }else{
                notification.contentView.setImageViewResource(R.id.imageViewAlbumArt, R.drawable.cover);
                if(currentVersionSupportBigNotification){
                    notification.bigContentView.setImageViewResource(R.id.imageViewAlbumArt, R.drawable.cover);
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }


        notification.contentView.setViewVisibility(R.id.stop, View.VISIBLE);
        notification.contentView.setViewVisibility(R.id.play, View.GONE);



        if(PlayerConstants.RADIO_STOP){
            notification.contentView.setViewVisibility(R.id.stop, View.GONE);
            notification.contentView.setViewVisibility(R.id.play, View.VISIBLE);

            if(currentVersionSupportBigNotification){
                notification.bigContentView.setViewVisibility(R.id.stop, View.GONE);
                notification.bigContentView.setViewVisibility(R.id.play, View.VISIBLE);
            }
        }else{
            notification.contentView.setViewVisibility(R.id.stop, View.VISIBLE);
            notification.contentView.setViewVisibility(R.id.play, View.GONE);

            if(currentVersionSupportBigNotification){
                notification.bigContentView.setViewVisibility(R.id.stop, View.VISIBLE);
                notification.bigContentView.setViewVisibility(R.id.play, View.GONE);
            }
        }



        if(PlayerConstants.RADIO_LIKE){
            notification.contentView.setViewVisibility(R.id.like, View.GONE);
            notification.contentView.setViewVisibility(R.id.like_on, View.VISIBLE);
            if(currentVersionSupportBigNotification){
                notification.bigContentView.setViewVisibility(R.id.like, View.GONE);
                notification.bigContentView.setViewVisibility(R.id.like_on, View.VISIBLE);
            }
        }else{
            notification.contentView.setViewVisibility(R.id.like, View.VISIBLE);
            notification.contentView.setViewVisibility(R.id.like_on, View.GONE);

            if(currentVersionSupportBigNotification){
                notification.bigContentView.setViewVisibility(R.id.like, View.VISIBLE);
                notification.bigContentView.setViewVisibility(R.id.like_on, View.GONE);
            }
        }

        notification.contentView.setTextViewText(R.id.textSongName, songName);
        notification.contentView.setTextViewText(R.id.textAlbumName, albumName);
        if(currentVersionSupportBigNotification){
            notification.bigContentView.setTextViewText(R.id.textSongName, songName);
            notification.bigContentView.setTextViewText(R.id.textAlbumName, albumName);
        }
        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        startForeground(NOTIFICATION_ID, notification);
    }

    /**
     * Notification click listeners
     * @param view
     */
    public void setListeners(RemoteViews view) {
        Intent close = new Intent(NOTIFY_CLOSE);
        Intent like = new Intent(NOTIFY_LIKE);
        Intent stop = new Intent(NOTIFY_STOP);
        Intent play = new Intent(NOTIFY_PLAY);

       // PendingIntent pPrevious = PendingIntent.getBroadcast(getApplicationContext(), 0, previous, PendingIntent.FLAG_UPDATE_CURRENT);
       // view.setOnClickPendingIntent(R.id.btnPrevious, pPrevious);

        PendingIntent pClose = PendingIntent.getBroadcast(getApplicationContext(), 0, close, PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.close, pClose);

        PendingIntent pLike = PendingIntent.getBroadcast(getApplicationContext(), 0, like, PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.like, pLike);

        PendingIntent pDisLike = PendingIntent.getBroadcast(getApplicationContext(), 0, like, PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.like_on, pDisLike);

        PendingIntent pStop = PendingIntent.getBroadcast(getApplicationContext(), 0, stop, PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.stop, pStop);

        PendingIntent pPlay = PendingIntent.getBroadcast(getApplicationContext(), 0, play, PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.play, pPlay);

    }


    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    private void RegisterRemoteClient(){
        remoteComponentName = new ComponentName(getApplicationContext(), new NotificationBroadcast().ComponentName());
        try {
            if(remoteControlClient == null) {
                audioManager.registerMediaButtonEventReceiver(remoteComponentName);
                Intent mediaButtonIntent = new Intent(Intent.ACTION_MEDIA_BUTTON);
                mediaButtonIntent.setComponent(remoteComponentName);
                PendingIntent mediaPendingIntent = PendingIntent.getBroadcast(this, 0, mediaButtonIntent, 0);
                remoteControlClient = new RemoteControlClient(mediaPendingIntent);
                audioManager.registerRemoteControlClient(remoteControlClient);
            }
            remoteControlClient.setTransportControlFlags(
                    RemoteControlClient.FLAG_KEY_MEDIA_PLAY |
                            RemoteControlClient.FLAG_KEY_MEDIA_PAUSE |
                            RemoteControlClient.FLAG_KEY_MEDIA_PLAY_PAUSE |
                            RemoteControlClient.FLAG_KEY_MEDIA_STOP |
                            RemoteControlClient.FLAG_KEY_MEDIA_PREVIOUS |
                            RemoteControlClient.FLAG_KEY_MEDIA_NEXT);
        }catch(Exception ex) {
        }
    }

    @SuppressLint("NewApi")
    private void UpdateMetadata(Radio data){
        if (remoteControlClient == null)
            return;
        RemoteControlClient.MetadataEditor metadataEditor = remoteControlClient.editMetadata(true);
        metadataEditor.putString(MediaMetadataRetriever.METADATA_KEY_ALBUM, "");
        metadataEditor.putString(MediaMetadataRetriever.METADATA_KEY_ARTIST, data.getName());
        metadataEditor.putString(MediaMetadataRetriever.METADATA_KEY_TITLE, data.getName());
        Bitmap albumArt = getBitmapFromURL(radioSeleted.getImage());
        if(albumArt == null){
            albumArt = BitmapFactory.decodeResource(getResources(), R.drawable.default_album_art);
        }
        metadataEditor.putBitmap(RemoteControlClient.MetadataEditor.BITMAP_KEY_ARTWORK, albumArt);
        metadataEditor.apply();
        audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
    }

    @Override
    public void onAudioFocusChange(int focusChange) {

    }
}
