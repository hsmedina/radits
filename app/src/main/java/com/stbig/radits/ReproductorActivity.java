package com.stbig.radits;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.RemoteControlClient;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.InflateException;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;
import com.stbig.radits.helper.ManagerModel;
import com.stbig.radits.helper.PlayerConstants;
import com.stbig.radits.helper.imagen.Utils;
import com.stbig.radits.objects.Radio;
import com.stbig.radits.services.APlayerService;
import com.truizlop.fabreveallayout.FABRevealLayout;
import com.truizlop.fabreveallayout.OnRevealChangeListener;

import jp.wasabeef.picasso.transformations.BlurTransformation;

public class ReproductorActivity extends AppCompatActivity implements View.OnClickListener {

    //private String sourceRadio;
    private static ImageView btnStop;
    private static ImageView btnLike;
    private static ImageButton btnRecord;
    private static TextView radioName;
    private static TextView songName;
    private static TextView programName;
    private ManagerModel model;
    private ImageView radioImage;


    private static FABRevealLayout fabRevealLayout;

    private Radio radioSeleted;
    private DisplayMetrics displaymetrics;
    private int heightScreen;
    private AdView mAdViewPlayer;
    private boolean flagNewRadio = true;

    private Intent acpServices;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_reproductor);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);


        try {

            radioName = (TextView) findViewById(R.id.radio_name);
            songName =  (TextView) findViewById(R.id.song_name);
            programName =  (TextView) findViewById(R.id.program_name);

            btnStop = (ImageView) findViewById(R.id.stop);
            btnLike = (ImageView) findViewById(R.id.like);
            radioImage = (ImageView) findViewById(R.id.image_radio);


            btnStop.setOnClickListener(this);
            btnLike.setOnClickListener(this);

            fabRevealLayout = (FABRevealLayout) findViewById(R.id.fab_reveal_layout);
            configureFABReveal(fabRevealLayout);

            model = new ManagerModel(this);

            acpServices = new Intent(this, APlayerService.class);

            mAdViewPlayer = (AdView) findViewById(R.id.ads_player);
            AdRequest adRequest = new AdRequest.Builder().addTestDevice("3DD2A0D1B293710BE8CF17DCB4BB8677").build();

            mAdViewPlayer.loadAd(adRequest);
            mAdViewPlayer.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    mAdViewPlayer.setVisibility(View.VISIBLE);
                }
            });




        } catch (InflateException e) {
            Log.e("error ReproductorAct",e.getMessage());
        }

    }


    @Override
    protected void onResume() {
        super.onResume();

        Bundle extra = getIntent().getExtras();

        if(extra!=null){
            radioSeleted = (Radio) extra.get(getString(R.string.entity_radio));
            loadInfoRadio(radioSeleted);
            programName.setText(getString(R.string.loading_radio));
            fabRevealLayout.revealSecondaryView();
        }

        registerReceiver(trackingServiceRadio, new IntentFilter(APlayerService.RADIO_STATUS_START));

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                //onBackPressed();
               // if(PlayerConstants.RADIO_STOP)
                 //   destroyRadio();

                Intent intent = new Intent(this,MainActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void configureFABReveal(FABRevealLayout fabRevealLayout) {




        fabRevealLayout.setOnRevealChangeListener(new OnRevealChangeListener() {
            @Override
            public void onMainViewAppeared(FABRevealLayout fabRevealLayout, View mainView) {

            }

            @Override
            public void onSecondaryViewAppeared(final FABRevealLayout fabRevealLayout, View secondaryView) {
               if(!flagNewRadio)
                   startRadio();

            }
        });
    }


   /* private void startRadio(){
        if(model.isPlaying()){
            stopService(acpServices);
        }

        programName.setText(getString(R.string.loading_radio));
        Bundle args = new Bundle();
        args.putSerializable("entity",radioSeleted);
        acpServices.putExtras(args);
        startService(acpServices);
    }

    */

    private void destroyRadio(){
        PlayerConstants.RADIO_STOP=true;
        programName.setText(getString(R.string.app_name));
        stopService(acpServices);
    }

    private void startRadio(){
        programName.setText(getString(R.string.loading_radio));
        Log.i("play", " button ");
        PlayerConstants.PLAY_STOP_HANDLER.sendMessage(PlayerConstants.PLAY_STOP_HANDLER.obtainMessage(0,getString(R.string.play)));
    }

    private void stopRadio(){
        Log.i("stop", " button ");
        flagNewRadio = false;
        programName.setText(getString(R.string.app_name));
        PlayerConstants.PLAY_STOP_HANDLER.sendMessage(PlayerConstants.PLAY_STOP_HANDLER.obtainMessage(0, getString(R.string.stop)));
    }

    public void likeRadio(){
        if(!PlayerConstants.RADIO_LIKE){
            PlayerConstants.RADIO_LIKE=true;
            model.likeRadio(radioSeleted.getId(), 1);
            Utils.animateLike(btnLike);
        }else{
            PlayerConstants.RADIO_LIKE=false;
            model.likeRadio(radioSeleted.getId(), 0);
            Utils.animateUnLike(btnLike);
        }
        PlayerConstants.RADIO_LIKE_HANDLER.sendMessage(PlayerConstants.RADIO_LIKE_HANDLER.obtainMessage());
    }

    public static void changeLikeNotification() {
        if(PlayerConstants.RADIO_LIKE){
            Utils.animateLike(btnLike);
        }else{
            Utils.animateUnLike(btnLike);
        }
    }

    public static void changeStopPlayNotification() {
        if(PlayerConstants.RADIO_STOP){
            fabRevealLayout.revealMainView();
        }else{
            fabRevealLayout.revealSecondaryView();
        }
    }

    private void prepareBackTransition(final FABRevealLayout fabRevealLayout) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fabRevealLayout.revealMainView();
            }
        }, 5000);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.stop:
                stopRadio();
                break;
            case R.id.like:
                likeRadio();
                break;
        }
    }

    private void loadInfoRadio(Radio currentRadio){

            radioName.setText(currentRadio.getName());
            songName.setText(currentRadio.getName());

            if(currentRadio.getFavorito()==0) {
                PlayerConstants.RADIO_LIKE=false;
                btnLike.setImageResource(R.drawable.ic_like_off);
            }else {
                PlayerConstants.RADIO_LIKE=true;
                btnLike.setImageResource(R.drawable.ic_like_on);
            }

        Picasso.with(this)
                .load(currentRadio.getImage())
                .transform(new BlurTransformation(this))
                .resize(Utils.dpToPx(this,400), Utils.dpToPx(this,400))
                .centerCrop()
                .into(radioImage);

    }


    private BroadcastReceiver trackingServiceRadio = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub

            int status = intent.getExtras().getInt(getString(R.string.radio_status));

            Log.e("result intent repro", String.valueOf(status));

            switch(status){
                case -1:
                    Log.i("broadcast Reciver error", " ok ");
                    fabRevealLayout.revealMainView();
                    break;
                case 1:
                    Log.i("broadcast Reciver play", " ok ");
                    if(!flagNewRadio)
                        fabRevealLayout.revealSecondaryView();


                    programName.setText(getString(R.string.app_name));
                    break;
                case 2:
                    fabRevealLayout.revealMainView();
                    Log.i("broadcast Reciver stop", " ok ");
                    break;
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(trackingServiceRadio);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    public static void revelSecundary(){
            fabRevealLayout.revealSecondaryView();
    }

}
