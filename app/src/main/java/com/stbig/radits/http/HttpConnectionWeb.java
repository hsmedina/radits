package com.stbig.radits.http;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;


import com.stbig.radits.helper.Files;
import com.stbig.radits.helper.imagen.Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by helbert on 16/05/15.
 */
public class HttpConnectionWeb {

    private HttpURLConnection conexion;

    private URL url;
    private String TAG_HTTP = "HttpDebug";
    private String link;
    private HashMap<String, String> params;


    public HttpConnectionWeb(){
    }

    public HttpConnectionWeb(String link){
        this.link=link;

        params = new HashMap<>();
    }


    public String connect() throws IOException {

        String linea;

        StringBuilder construye = new StringBuilder();

        try{
            //Log.i("URL IS",link + getPostDataString(params));

            this.url= new URL(link + getPostDataString(params));


            conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
           // conexion.setDoInput(true);
           // conexion.setDoOutput(true);

           // Log.i(TAG_HTTP, "parametros " + getPostDataString(params));

           // PrintWriter printer = new PrintWriter(conexion.getOutputStream());
           // printer.print(getPostDataString(params));
           // printer.close();

            InputStreamReader input = new InputStreamReader(conexion.getInputStream());
            BufferedReader buffer = new BufferedReader(input);

            //Log.i(TAG_HTTP, String.valueOf(conexion.getResponseCode()));

            if(conexion.getResponseCode()==HttpURLConnection.HTTP_OK){

                while((linea=buffer.readLine()) !=null){
                    construye.append(linea);
                }

                return construye.toString();

            }

            return "";

        }catch(Exception e){
            Log.e(TAG_HTTP, e.getMessage());
            return "";
        }

    }

    public void AddParam(String name, String value) {
        params.put(name, value);
    }

    private String getPostDataString(HashMap<String, String> paramsPost) throws UnsupportedEncodingException{
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : paramsPost.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    public void downloadFile(String url){

       try {

           String characters[] = url.split("/");
           String nameImagen = characters[characters.length-1];

           Files fileDir = new Files();
           fileDir.setNameFiles(nameImagen);


           File f = new File (fileDir.getPathService());
           if (f.exists ()) return;

            Bitmap bitmap=null;
            URL imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection)imageUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is=conn.getInputStream();
            OutputStream os = new FileOutputStream(f);
            Utils.CopyStream(is, os);
            os.close();
           // bitmap = decodeFile(f,100,100);
           // return bitmap;
        } catch (Throwable ex){
            ex.printStackTrace();
            //return null;
        }
    }


    public boolean downloadRadio(String url){

        try {

            Files fileDir = new Files();
            fileDir.setNameFiles("listen.mp3");


            File f = new File (fileDir.getPathService());
            if (f.exists()) {
                f.delete();
                f.createNewFile();
            }

            URL radioUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection)radioUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is=conn.getInputStream();
            OutputStream os = new FileOutputStream(f);
            Utils.CopyStream(is, os);
            os.close();
            Log.i("fuente", "despues de osclose");
            return true;
        } catch (Throwable ex){
            ex.printStackTrace();
            return false;
        }
    }


}

