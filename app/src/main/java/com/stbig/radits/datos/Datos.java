package com.stbig.radits.datos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.stbig.radits.R;
import com.stbig.radits.helper.AlphabetGroup;
import com.stbig.radits.helper.AlphabetGroupCalculator;
import com.stbig.radits.helper.DBitem;
import com.stbig.radits.objects.Extra;
import com.stbig.radits.objects.Pais;
import com.stbig.radits.objects.Radio;

import java.util.ArrayList;

/**
 * Created by root on 23/11/15.
 */
public class Datos extends SQLiteOpenHelper {


    private SQLiteDatabase db=null;
    private Context context;

    private static final String CREATE_RADIO = "CREATE TABLE " + DBitem.TABLE_RADIO + " (" + DBitem.RADIO_ID + " TEXT," + DBitem.RADIO_CATEGORY + " TEXT,"  + DBitem.RADIO_NOMBRE + " TEXT," + DBitem.RADIO_IMAGE + " TEXT," + DBitem.RADIO_URL +  " TEXT," + DBitem.RADIO_ISO  + " TEXT," + DBitem.RADIO_GENERO  + " TEXT,"  + DBitem.RADIO_STATUS  + " TEXT," + DBitem.RADIO_TUNEIS_ID +  " TEXT,"  + DBitem.RADIO_CATEGORI_FILL +  " TEXT,"  + DBitem.RADIO_FAVORITO +  " INTEGER," + DBitem.RADIO_RECIENTE + " TEXT," + DBitem.RADIO_PLAYING + " INTEGER," + DBitem.RADIO_VIEW + " INTEGER," + DBitem.RADIO_SELECTED + " INTEGER);";
    private static final String CREATE_EXTRA = "CREATE TABLE " + DBitem.TABLE_EXTRA + " (" + DBitem.EXTRA_ID + " TEXT," + DBitem.EXTRA_LOCALE + " TEXT,"  + DBitem.EXTRA_NOMBRE+ " TEXT,"   + DBitem.EXTRA_TYPE + " TEXT," + DBitem.EXTRA_TUN + " TEXT," + DBitem.EXTRA_TUN_PARENT +  " TEXT," + DBitem.EXTRA_ISO  + " TEXT);";

    public Datos(Context context){
        super(context, context.getString(R.string.database), null, context.getResources().getInteger(R.integer.database));
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();

        try {
            db.execSQL(CREATE_RADIO);
            db.execSQL(CREATE_EXTRA);
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            throw e;
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void registerRadio(String id, String category, String name, String image, String url, String iso, String genre,
                              String status, String tunein, String category_fill, int favorito, String reciente, int isplaying, int views, int selected) throws SQLException {

        try {
                 
                db = getReadableDatabase();

            if(checkRegister(DBitem.TABLE_RADIO,DBitem.RADIO_ID,id)==0){
                ContentValues values = new ContentValues();
                values.put(DBitem.RADIO_ID, id);
                values.put(DBitem.RADIO_CATEGORY, category);
                values.put(DBitem.RADIO_NOMBRE, name);
                values.put(DBitem.RADIO_IMAGE, image);
                values.put(DBitem.RADIO_URL, url);
                values.put(DBitem.RADIO_ISO, iso);
                values.put(DBitem.RADIO_GENERO, genre);
                values.put(DBitem.RADIO_STATUS, status);
                values.put(DBitem.RADIO_TUNEIS_ID, tunein);
                values.put(DBitem.RADIO_CATEGORI_FILL, category_fill);
                values.put(DBitem.RADIO_FAVORITO, favorito);
                values.put(DBitem.RADIO_RECIENTE, reciente);
                values.put(DBitem.RADIO_PLAYING, isplaying);
                values.put(DBitem.RADIO_VIEW, views);
                values.put(DBitem.RADIO_SELECTED, selected);
                db.insert(DBitem.TABLE_RADIO, null, values);
            }

            /*else{
                String[] args = {id};
                ContentValues values = new ContentValues();
                values.put(DBitem.RADIO_CATEGORY, category);
                values.put(DBitem.RADIO_NOMBRE, name);
                values.put(DBitem.RADIO_IMAGE, image);
                values.put(DBitem.RADIO_URL, url);
                values.put(DBitem.RADIO_ISO, iso);
                values.put(DBitem.RADIO_GENERO, genre);
                values.put(DBitem.RADIO_STATUS, status);
                values.put(DBitem.RADIO_TUNEIS_ID, tunein);
                values.put(DBitem.RADIO_CATEGORI_FILL, category_fill);
                values.put(DBitem.RADIO_FAVORITO, favorito);
                values.put(DBitem.RADIO_RECIENTE, reciente);
                values.put(DBitem.RADIO_PLAYING, isplaying);
                values.put(DBitem.RADIO_VIEW, views);
                db.update(DBitem.TABLE_RADIO, values, DBitem.RADIO_ID + "=?", args);
            }*/

            db.close();

        } catch (SQLException e) {

        }
    }


    public void registerViewsRadio(String id, int views) throws SQLException {

        try {
                 
                db = getReadableDatabase();

            if(checkRegister(DBitem.TABLE_RADIO,DBitem.RADIO_ID,id)!=0){
                String[] args = {id};
                ContentValues values = new ContentValues();
                values.put(DBitem.RADIO_VIEW, views);
                db.update(DBitem.TABLE_RADIO, values, DBitem.RADIO_ID + "=?", args);
            }

            db.close();

        } catch (SQLException e) {

        }
    }

    public Radio getRadioSelected() throws SQLException {
        Radio r = null;

             
            db = getReadableDatabase();

        Cursor c = db.rawQuery("select * from " + DBitem.TABLE_RADIO + " where " + DBitem.RADIO_SELECTED + "=1" , null);
        if (c.getCount() != 0) {
            c.moveToFirst();
            do {
                r = new Radio(c.getString(0), c.getString(1), c.getString(2), c.getString(3), c.getString(4), c.getString(5), c.getString(6), c.getString(7), c.getString(8) , c.getString(9) , c.getInt(10), c.getString(11), c.getInt(12), c.getInt(13), c.getInt(14));
            } while (c.moveToNext());
        }
        db.close();
        c.close();
        return r;
    }

    public void setSelectedRadio(String id, int seleted) throws SQLException {

        try {
            cleanPlayingRadio();

                 
                db = getReadableDatabase();

            String[] args = {id};
            ContentValues values = new ContentValues();
            values.put(DBitem.RADIO_SELECTED, seleted);
            db.update(DBitem.TABLE_RADIO, values, DBitem.RADIO_ID + "=?", args);
            db.close();
        } catch (SQLException e) {
            Log.e("error selected",e.getMessage());
        }
    }

    public Radio getStatusPlayerRadio(String id) throws SQLException {
        Radio r = null;

             
            db = getReadableDatabase();

        Cursor c = db.rawQuery("select * from " + DBitem.TABLE_RADIO + " where " + DBitem.RADIO_ID + "='" + id + "'", null);
        if (c.getCount() != 0) {
            c.moveToFirst();
            do {
                r = new Radio(c.getString(0), c.getString(1), c.getString(2), c.getString(3), c.getString(4), c.getString(5), c.getString(6), c.getString(7), c.getString(8) , c.getString(9) , c.getInt(10), c.getString(11), c.getInt(12), c.getInt(13), c.getInt(14));
            } while (c.moveToNext());
        }
        db.close();
        c.close();
        return r;
    }

    public void setStatusPlayerRadio(String id, int status) throws SQLException {

        try {

            db = getReadableDatabase();
            String[] args = {id};
            ContentValues values = new ContentValues();
            values.put(DBitem.RADIO_PLAYING, status);
            db.update(DBitem.TABLE_RADIO, values, DBitem.RADIO_ID + "=?", args);
            db.close();
        } catch (SQLException e) {
            Log.e("error inser playing",e.getMessage());
        }
    }

    public void setRecienteRadio(String id, String date) throws SQLException {

        try {

                 
                db = getReadableDatabase();

            String[] args = {id};
            ContentValues values = new ContentValues();
            values.put(DBitem.RADIO_RECIENTE, date);
            db.update(DBitem.TABLE_RADIO, values, DBitem.RADIO_ID + "=?", args);
            db.close();

        } catch (SQLException e) {
            Log.e("error inser reicente",e.getMessage());
        }
    }

    public void setLikeRadio(String id, int like) throws SQLException {

        try {

                 
                db = getReadableDatabase();


            String[] args = {id};
            ContentValues values = new ContentValues();
            values.put(DBitem.RADIO_FAVORITO, like);
            db.update(DBitem.TABLE_RADIO, values, DBitem.RADIO_ID + "=?", args);
            db.close();

        } catch (SQLException e) {

        }
    }

    public void cleanPlayingRadio() throws SQLException {

        try {

                 
                db = getReadableDatabase();

            String[] args = {String.valueOf(1)};
            ContentValues values = new ContentValues();
            values.put(DBitem.RADIO_PLAYING, 0);
            values.put(DBitem.RADIO_SELECTED, 0);
            db.update(DBitem.TABLE_RADIO, values, null, null);
            db.close();

        } catch (SQLException e) {

        }
    }

    public ArrayList<Radio> listarRadios(){
        ArrayList<Radio> values = new ArrayList<Radio>();

             
            db = getReadableDatabase();

        Cursor c = db.rawQuery("select * from " + DBitem.TABLE_RADIO + " where " + DBitem.RADIO_CATEGORI_FILL + "='' order by " + DBitem.RADIO_NOMBRE + " asc ", null);
        if (c.getCount() != 0) {
            c.moveToFirst();
            do {
                Radio data = new Radio(c.getString(0), c.getString(1), c.getString(2), c.getString(3), c.getString(4), c.getString(5), c.getString(6), c.getString(7), c.getString(8) , c.getString(9) , c.getInt(10), c.getString(11), c.getInt(12), c.getInt(13), c.getInt(14));
                values.add(data);
            } while (c.moveToNext());
        }
        db.close();
        c.close();
        return values;
    }


    public ArrayList<Radio> listarRadiosBySearch(String query){
        ArrayList<Radio> values = new ArrayList<Radio>();

           
            db = getReadableDatabase();

        Cursor c = db.rawQuery("select * from " + DBitem.TABLE_RADIO + " where " + DBitem.RADIO_CATEGORI_FILL + "='' and " + DBitem.RADIO_NOMBRE +  " like '" + query + "%'" , null);
        Log.e("total busquedad",String.valueOf(c.getCount()));
        if (c.getCount() != 0) {
            c.moveToFirst();
            do {
                Radio data = new Radio(c.getString(0), c.getString(1), c.getString(2), c.getString(3), c.getString(4), c.getString(5), c.getString(6), c.getString(7), c.getString(8) , c.getString(9) , c.getInt(10), c.getString(11), c.getInt(12), c.getInt(13), c.getInt(14));
                values.add(data);
            } while (c.moveToNext());
        }
        db.close();
        c.close();
        return values;
    }


    public ArrayList<Radio> listarPopulares(){
        ArrayList<Radio> values = new ArrayList<Radio>();

             
            db = getReadableDatabase();

        Cursor c = db.rawQuery("select * from " + DBitem.TABLE_RADIO + " where " + DBitem.RADIO_VIEW + ">0 order by " + DBitem.RADIO_VIEW + " desc ", null);
        if (c.getCount() != 0) {
            c.moveToFirst();
            do {
                Radio data = new Radio(c.getString(0), c.getString(1), c.getString(2), c.getString(3), c.getString(4), c.getString(5), c.getString(6), c.getString(7), c.getString(8) , c.getString(9) , c.getInt(10), c.getString(11), c.getInt(12), c.getInt(13), c.getInt(14));
                values.add(data);
            } while (c.moveToNext());
        }
        db.close();
        c.close();
        return values;
    }

    public ArrayList<Radio> listarFavoritos(){
        ArrayList<Radio> values = new ArrayList<Radio>();

         
            db = getReadableDatabase();

        Cursor c = db.rawQuery("select * from " + DBitem.TABLE_RADIO + " where " + DBitem.RADIO_FAVORITO + "=1" , null);

        if (c.getCount() != 0) {
            c.moveToFirst();
            do {
                Radio data = new Radio(c.getString(0), c.getString(1), c.getString(2), c.getString(3), c.getString(4), c.getString(5), c.getString(6), c.getString(7), c.getString(8) , c.getString(9) , c.getInt(10), c.getString(11), c.getInt(12), c.getInt(13), c.getInt(14));
                values.add(data);
            } while (c.moveToNext());
        }
        db.close();
        c.close();
        return values;
    }

    public ArrayList<Radio> listarRecientes(){
        ArrayList<Radio> values = new ArrayList<Radio>();

             
            db = getReadableDatabase();

        Cursor c = db.rawQuery("select * from " + DBitem.TABLE_RADIO + " where " + DBitem.RADIO_RECIENTE + "!='' order by " + DBitem.RADIO_RECIENTE + " desc LIMIT 10", null);
        if (c.getCount() != 0) {
            c.moveToFirst();
            do {
                Radio data = new Radio(c.getString(0), c.getString(1), c.getString(2), c.getString(3), c.getString(4), c.getString(5), c.getString(6), c.getString(7), c.getString(8) , c.getString(9) , c.getInt(10), c.getString(11), c.getInt(12), c.getInt(13), c.getInt(14));
                values.add(data);
            } while (c.moveToNext());
        }
        db.close();
        c.close();
        return values;
    }


    public ArrayList<Radio> listarRadiosByCategory(String category){
        ArrayList<Radio> values = new ArrayList<Radio>();

             
            db = getReadableDatabase();

        Cursor c = db.rawQuery("select * from " + DBitem.TABLE_RADIO  + " where " + DBitem.RADIO_CATEGORI_FILL + "='" + category + "'" , null);
        if (c.getCount() != 0) {
            c.moveToFirst();
            do {
                Radio data = new Radio(c.getString(0), c.getString(1), c.getString(2), c.getString(3), c.getString(4), c.getString(5), c.getString(6), c.getString(7), c.getString(8) , c.getString(9) , c.getInt(10), c.getString(11), c.getInt(12), c.getInt(13), c.getInt(14));
                values.add(data);
            } while (c.moveToNext());
        }
        db.close();
        c.close();
        return values;
    }

    public ArrayList<Radio> listarRadiosByCategorySearch(String category, String query){
        ArrayList<Radio> values = new ArrayList<Radio>();

             
            db = getReadableDatabase();

        Cursor c = db.rawQuery("select * from " + DBitem.TABLE_RADIO  + " where " + DBitem.RADIO_CATEGORI_FILL + "='" + category + "' and " + DBitem.RADIO_NOMBRE + " like '%" + query + "%'" , null);
        if (c.getCount() != 0) {
            c.moveToFirst();
            do {
                Radio data = new Radio(c.getString(0), c.getString(1), c.getString(2), c.getString(3), c.getString(4), c.getString(5), c.getString(6), c.getString(7), c.getString(8) , c.getString(9) , c.getInt(10), c.getString(11), c.getInt(12), c.getInt(13), c.getInt(14));
                values.add(data);
            } while (c.moveToNext());
        }
        db.close();
        c.close();
        return values;
    }

    public ArrayList<Radio> listarRadiosByPais(String iso_country){
        ArrayList<Radio> values = new ArrayList<Radio>();

             
            db = getReadableDatabase();


        Cursor c = db.rawQuery("select * from " + DBitem.TABLE_RADIO  + " where " + DBitem.RADIO_ISO + "='" + iso_country + "'" , null);
        if (c.getCount() != 0) {
            c.moveToFirst();
            do {
                Radio data = new Radio(c.getString(0), c.getString(1), c.getString(2), c.getString(3), c.getString(4), c.getString(5), c.getString(6), c.getString(7), c.getString(8) , c.getString(9) , c.getInt(10), c.getString(11), c.getInt(12), c.getInt(13), c.getInt(14));
                values.add(data);
            } while (c.moveToNext());
        }
        db.close();
        c.close();
        return values;
    }

    public ArrayList<Radio> listarRadiosByPaisSearch(String iso_country, String query){
        ArrayList<Radio> values = new ArrayList<Radio>();

             
            db = getReadableDatabase();


        Cursor c = db.rawQuery("select * from " + DBitem.TABLE_RADIO  + " where " + DBitem.RADIO_ISO + "='" + iso_country + "' and " + DBitem.RADIO_NOMBRE + " like '%" + query + "%'"  , null);
        if (c.getCount() != 0) {
            c.moveToFirst();
            do {
                Radio data = new Radio(c.getString(0), c.getString(1), c.getString(2), c.getString(3), c.getString(4), c.getString(5), c.getString(6), c.getString(7), c.getString(8) , c.getString(9) , c.getInt(10), c.getString(11), c.getInt(12), c.getInt(13), c.getInt(14));
                values.add(data);
            } while (c.moveToNext());
        }
        db.close();
        c.close();
        return values;
    }




    public void registerExtras(String id, String locale, String nombre, String type, String tun, String tun_parent,  String iso) throws SQLException {

        try {
                 
                db = getReadableDatabase();

            if(checkRegister(DBitem.TABLE_EXTRA,DBitem.EXTRA_ID,String.valueOf(id))==0){
                ContentValues values = new ContentValues();
                values.put(DBitem.EXTRA_ID, id);
                values.put(DBitem.EXTRA_LOCALE, locale);
                values.put(DBitem.EXTRA_NOMBRE, nombre);
                values.put(DBitem.EXTRA_TYPE, type);
                values.put(DBitem.EXTRA_TUN, tun);
                values.put(DBitem.EXTRA_TUN_PARENT, tun_parent);
                values.put(DBitem.EXTRA_ISO, iso);
                db.insert(DBitem.TABLE_EXTRA, null, values);
            }else{
                String[] args = {id};
                ContentValues values = new ContentValues();
                values.put(DBitem.EXTRA_LOCALE, locale);
                values.put(DBitem.EXTRA_NOMBRE, nombre);
                values.put(DBitem.EXTRA_TYPE, type);
                values.put(DBitem.EXTRA_TUN, tun);
                values.put(DBitem.EXTRA_TUN_PARENT, tun_parent);
                values.put(DBitem.EXTRA_ISO, iso);
                db.update(DBitem.TABLE_EXTRA, values, DBitem.EXTRA_ID + "=?", args);
            }

            db.close();

        } catch (SQLException e) {

        }
    }

    public ArrayList<Extra> listarExtras(String type){
        ArrayList<Extra> values = new ArrayList<Extra>();

             
            db = getReadableDatabase();


        Cursor c = db.rawQuery("select * from " + DBitem.TABLE_EXTRA + " where "  + DBitem.EXTRA_TYPE + "='" + type + "'", null);
        if (c.getCount() != 0) {
            c.moveToFirst();
            do {
                Extra data = new Extra(c.getString(0), c.getString(1), c.getString(2), c.getString(3), c.getString(4), c.getString(5), c.getString(6));
                values.add(data);
            } while (c.moveToNext());
        }
        db.close();
        c.close();
        return values;
    }

    public ArrayList<Extra> listarExtrasPaisesBySearch(String type, String query){
        ArrayList<Extra> values = new ArrayList<Extra>();

         
            db = getReadableDatabase();

        Cursor c = db.rawQuery("select * from " + DBitem.TABLE_EXTRA + " where "  + DBitem.EXTRA_TYPE + "='" + type + "' and " + DBitem.EXTRA_NOMBRE +  " like '" + query + "%'", null);
        if (c.getCount() != 0) {
            c.moveToFirst();
            do {
                Extra data = new Extra(c.getString(0), c.getString(1), c.getString(2), c.getString(3), c.getString(4), c.getString(5), c.getString(6));
                values.add(data);
            } while (c.moveToNext());
        }
        db.close();
        c.close();
        return values;
    }



    public int checkRegister(String table,String field,String data) {

             
            db = getReadableDatabase();
        
        int total=0;
        String[] param = {data};
        Cursor c = db.rawQuery("select * from " + table + "  where " + field + "=?", param);
        total = c.getCount();
        c.close();
        return total;
    }

}
