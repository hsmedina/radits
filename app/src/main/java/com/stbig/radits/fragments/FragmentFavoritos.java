package com.stbig.radits.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.stbig.radits.R;
import com.stbig.radits.ReproductorActivity;
import com.stbig.radits.adapters.AdapterRadios;
import com.stbig.radits.datos.AppPreferences;
import com.stbig.radits.datos.Datos;
import com.stbig.radits.helper.ManagerModel;
import com.stbig.radits.helper.PlayerConstants;
import com.stbig.radits.helper.imagen.Utils;
import com.stbig.radits.listener.ItemClickSupport;
import com.stbig.radits.services.APlayerService;

import jp.wasabeef.recyclerview.animators.adapters.SlideInBottomAnimationAdapter;

/**
 * Created by root on 19/11/15.
 */
public class FragmentFavoritos extends Fragment {

    private Activity activity;
    private Context context;
    private static View view;
    private static RecyclerView recyclerView;
    private AppPreferences preferences;
    private Datos data;
    private static AdapterRadios adapter;
    private static final String TEXT_FRAGMENT = "FRAGMENT_FAVORITOS";
    private LinearLayout empty;
    private Intent acpServices;
    private ManagerModel model;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity=getActivity();
        context=activity.getApplicationContext();
      //  setHasOptionsMenu(true);
    }

    public static MainFragment newInstance(String text){
        MainFragment mFragment = new MainFragment();
        Bundle mBundle = new Bundle();
        mBundle.putString(TEXT_FRAGMENT, text);
        mFragment.setArguments(mBundle);
        return mFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_favoritos, container, false);
            recyclerView = (RecyclerView) view.findViewById(R.id.grid_radios);
            empty = (LinearLayout) view.findViewById(R.id.empty_content);

            preferences = new AppPreferences(context);
            data= new Datos(context);
            model = new ManagerModel(context);
            acpServices = new Intent(context, APlayerService.class);

            changeModeRecycler(preferences.getValueBoolean(context.getString(R.string.mode_list)));

            ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                @Override
                public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                    if (data.listarFavoritos().get(position).getIsplaying() != 1) {


                        if(Utils.isConnectingToInternet(context)){

                            stopRadio();
                            PlayerConstants.RADIO_STOP = false;
                            acpServices.putExtras(getRadio(position));
                            context.startService(acpServices);

                            Intent intent = new Intent(context, ReproductorActivity.class);
                            intent.putExtras(getRadio(position));
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                        }

                    }

                }
            });


        } catch (InflateException e) {

        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateFavoritos();
    }



    public void updateFavoritos(){

        adapter = new AdapterRadios(context,data.listarFavoritos(),preferences.getValueBoolean(context.getString(R.string.mode_list)));
        recyclerView.setAdapter(new SlideInBottomAnimationAdapter(adapter));

            if(data.listarFavoritos().size()>0){
                empty.setVisibility(View.GONE);
            }else{
                empty.setVisibility(View.VISIBLE);
            }
        adapter.notifyDataSetChanged();
    }


    private Bundle getRadio(int position){
        Bundle args = new Bundle();
        args.putSerializable(getString(R.string.entity_radio), data.listarFavoritos().get(position));
        return args;
    }

    public void changeModeRecycler(boolean mode){

        adapter = new AdapterRadios(context,data.listarFavoritos(),mode);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if(!mode){
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        }else{
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        }

        recyclerView.setAdapter(new SlideInBottomAnimationAdapter(adapter));

    }

    private void stopRadio(){
        model.closePlayingRadio();
        context.stopService(acpServices);
    }

    public static void refreshFavorites() {
        recyclerView.setAdapter(new SlideInBottomAnimationAdapter(adapter));
    }

}
