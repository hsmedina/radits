package com.stbig.radits.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.andraskindler.quickscroll.QuickScroll;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.stbig.radits.R;
import com.stbig.radits.adapters.AdapterExtra;
import com.stbig.radits.adapters.AdapterPais;
import com.stbig.radits.datos.AppPreferences;
import com.stbig.radits.datos.Datos;
import com.stbig.radits.listener.ItemClickGeneroSupport;
import com.stbig.radits.listener.ItemClickSupport;
import com.stbig.radits.objects.Extra;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.animators.adapters.SlideInBottomAnimationAdapter;


/**
 * Created by root on 19/11/15.
 */
public class FragmentPaises extends Fragment {

    private Activity activity;
    private Context context;
    private static View view;
    private RecyclerView recyclerView;
    private Datos data;
    private AdapterExtra adapter;
    private static final String TEXT_FRAGMENT = "TEXT_FRAGMENT";
    private ItemClickGeneroSupport listenGenero;
    private AppPreferences preferences;
    private AdView mAdViewHome;
    private ArrayList<Extra> dataExtras;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity=getActivity();
        context=activity.getApplicationContext();
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity a;

        if (context instanceof Activity){
            a=(Activity) context;
            listenGenero = (ItemClickGeneroSupport) a;
        }

    }

    public static MainFragment newInstance(String text){
        MainFragment mFragment = new MainFragment();
        Bundle mBundle = new Bundle();
        mBundle.putString(TEXT_FRAGMENT, text);
        mFragment.setArguments(mBundle);
        return mFragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }


        view = inflater.inflate(R.layout.fragment_paises, null, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.list_pais);

        preferences = new AppPreferences(context);
        data= new Datos(context);
        setupRecyclerDefault();
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                preferences.saveValueInt(context.getString(R.string.drawer_item), 4);
                listenGenero.onSelectedListerGenero(dataExtras.get(position), v);

            }
        });

        mAdViewHome = (AdView) view.findViewById(R.id.ads_home);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("3DD2A0D1B293710BE8CF17DCB4BB8677").build();
        mAdViewHome.loadAd(adRequest);
        mAdViewHome.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdViewHome.setVisibility(View.VISIBLE);
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        preferences.saveValueInt(context.getString(R.string.drawer_item), 0);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        activity.getMenuInflater().inflate(R.menu.menu_pais, menu);

        SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();

        SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextChange(String newText){

                if(newText.length()==0){
                    setupRecyclerDefault();
                }

                return true;
            }
            @Override
            public boolean onQueryTextSubmit(String query){

                setupRecyclerSearch(query);
                return true;
            }

        };
        searchView.setOnQueryTextListener(textChangeListener);
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                setupRecyclerDefault();
                return false;
            }
        });


    }



    private void setupRecyclerDefault(){
        dataExtras = data.listarExtras("country");
        adapter = new AdapterExtra(context,data.listarExtras("country"));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.setAdapter(new SlideInBottomAnimationAdapter(adapter));
        recyclerView.setAdapter(adapter);
    }


    private void setupRecyclerSearch(String query){
        dataExtras = data.listarExtrasPaisesBySearch("country", query);
        adapter = new AdapterExtra(context,data.listarExtrasPaisesBySearch("country", query));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.setAdapter(new SlideInBottomAnimationAdapter(adapter));
        recyclerView.setAdapter(adapter);
    }

}
