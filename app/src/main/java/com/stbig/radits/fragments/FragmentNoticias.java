package com.stbig.radits.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.stbig.radits.R;
import com.stbig.radits.adapters.AdapterExtra;
import com.stbig.radits.datos.AppPreferences;
import com.stbig.radits.datos.Datos;
import com.stbig.radits.listener.ItemClickGeneroSupport;
import com.stbig.radits.listener.ItemClickSupport;

import jp.wasabeef.recyclerview.animators.adapters.SlideInBottomAnimationAdapter;

/**
 * Created by root on 19/11/15.
 */
public class FragmentNoticias extends Fragment {

    private Activity activity;
    private Context context;
    private static View view;
    private RecyclerView recyclerView;
    private Datos data;
    private AdapterExtra adapter;
    private static final String TEXT_FRAGMENT = "TEXT_FRAGMENT";
    private ItemClickGeneroSupport listenGenero;
    private AppPreferences preferences;
    private AdView mAdViewHome;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity=getActivity();
        context=activity.getApplicationContext();
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity a;

        if (context instanceof Activity){
            a=(Activity) context;
            listenGenero = (ItemClickGeneroSupport) a;
        }

    }

    public static MainFragment newInstance(String text){
        MainFragment mFragment = new MainFragment();
        Bundle mBundle = new Bundle();
        mBundle.putString(TEXT_FRAGMENT, text);
        mFragment.setArguments(mBundle);
        return mFragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }

        //try {
        view = inflater.inflate(R.layout.fragment_noticias, null, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.list_noticia);

        preferences = new AppPreferences(context);
        data= new Datos(context);
        adapter = new AdapterExtra(context,data.listarExtras("talk"));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.setAdapter(new SlideInBottomAnimationAdapter(adapter));

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                preferences.saveValueInt(context.getString(R.string.drawer_item), 6);
                listenGenero.onSelectedListerGenero(data.listarExtras("talk").get(position), v);

            }
        });

        mAdViewHome = (AdView) view.findViewById(R.id.ads_home);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("3DD2A0D1B293710BE8CF17DCB4BB8677").build();
        mAdViewHome.loadAd(adRequest);
        mAdViewHome.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdViewHome.setVisibility(View.VISIBLE);
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        preferences.saveValueInt(context.getString(R.string.drawer_item), 0);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

}
