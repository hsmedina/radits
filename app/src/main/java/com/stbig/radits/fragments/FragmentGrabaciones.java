package com.stbig.radits.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.stbig.radits.R;
import com.stbig.radits.adapters.AdapterRadios;
import com.stbig.radits.datos.Datos;

/**
 * Created by root on 19/11/15.
 */
public class FragmentGrabaciones extends Fragment {

    private Activity activity;
    private Context context;
    private static View view;
    private GridView lista;
    private Datos data;
    private AdapterRadios adapter;
    private static final String TEXT_FRAGMENT = "TEXT_FRAGMENT";
    private AdView mAdViewHome;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity=getActivity();
        context=activity.getApplicationContext();
        setHasOptionsMenu(true);
    }

    public static MainFragment newInstance(String text){
        MainFragment mFragment = new MainFragment();
        Bundle mBundle = new Bundle();
        mBundle.putString(TEXT_FRAGMENT, text);
        mFragment.setArguments(mBundle);
        return mFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_grabaciones, container, false);
            //lista = (GridView) view.findViewById(R.id.grid_radios);
            data= new Datos(context);


        } catch (InflateException e) {

        }

        mAdViewHome = (AdView) view.findViewById(R.id.ads_home);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("3DD2A0D1B293710BE8CF17DCB4BB8677").build();
        mAdViewHome.loadAd(adRequest);
        mAdViewHome.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdViewHome.setVisibility(View.VISIBLE);
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        //adapter = new AdapterRadios(context,data.listarRadios());
        //lista.setAdapter(adapter);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }
}
