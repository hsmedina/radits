package com.stbig.radits.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.stbig.radits.R;
import com.stbig.radits.adapters.TabPagerItem;
import com.stbig.radits.adapters.ViewPagerAdapter;
import com.stbig.radits.datos.AppPreferences;
import com.stbig.radits.objects.Radio;

import java.util.ArrayList;
import java.util.List;


public class ViewPagerFragment extends Fragment {
	private List<TabPagerItem> mTabs = new ArrayList<>();
    private AppPreferences preferences;
    private Activity activity;
    private Context context;
    private FragmentRadios fgLocales;
    private FragmentFavoritos fgFavoritos;
    private FragmentRecientes fgRecientes;

    private AdView mAdViewHome;
    private ViewPager mViewPager;
    private static int page=0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createTabPagerItem();
        activity=getActivity();
        context=activity.getApplicationContext();
    }

    private void createTabPagerItem(){
        mTabs.add(new TabPagerItem(getString(R.string.nav_1_1), new FragmentRadios()));
        mTabs.add(new TabPagerItem(getString(R.string.nav_2), new FragmentFavoritos()));
        mTabs.add(new TabPagerItem(getString(R.string.nav_3_3), new FragmentRecientes()));
        //mTabs.add(new TabPagerItem(getString(R.string.nav_3_3), MainFragment.newInstance(getString(R.string.nav_3_3))));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_viewpager, container, false);

        page = getArguments().getInt("page");

        mAdViewHome = (AdView) rootView.findViewById(R.id.ads_home);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("3DD2A0D1B293710BE8CF17DCB4BB8677").build();
        mAdViewHome.loadAd(adRequest);
        mAdViewHome.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdViewHome.setVisibility(View.VISIBLE);
            }
        });

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
    	mViewPager = (ViewPager) view.findViewById(R.id.viewPager);

    	mViewPager.setOffscreenPageLimit(mTabs.size());
        mViewPager.setAdapter(new ViewPagerAdapter(getChildFragmentManager(), mTabs));
        TabLayout mSlidingTabLayout = (TabLayout) view.findViewById(R.id.tabLayout);

        preferences = new AppPreferences(context);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
           // mSlidingTabLayout.setElevation(15);
        }

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch(position){
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });




        mSlidingTabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    public void onResume() {
        super.onResume();
        preferences.saveValueInt(getString(R.string.drawer_item), 0);

        mViewPager.setCurrentItem(page);

    }

    public void refreshFavoritesFragment(){
        fgFavoritos = (FragmentFavoritos) mTabs.get(1).getFragment();
        fgFavoritos.updateFavoritos();
        Log.i("refresh", "refresh fragment viewpager");
    }

    public void refreshModeFragment(boolean mode){
        fgLocales = (FragmentRadios) mTabs.get(0).getFragment();
        fgFavoritos = (FragmentFavoritos) mTabs.get(1).getFragment();
        fgRecientes = (FragmentRecientes) mTabs.get(2).getFragment();
        fgLocales.changeModeRecycler(mode);
        fgFavoritos.changeModeRecycler(mode);
        fgRecientes.changeModeRecycler(mode);
        Log.i("refresh","refresh mode fragment viewpager");
    }


    public void searchRadioLocales(ArrayList<Radio> radios,boolean mode){
        fgLocales = (FragmentRadios) mTabs.get(0).getFragment();
        fgLocales.searchRadio(radios, mode);
    }

    public void restoreRadioLocales(boolean mode){
        fgLocales = (FragmentRadios) mTabs.get(0).getFragment();
        fgLocales.changeModeRecycler(mode);
    }

    public void showFavorites(){
        mViewPager.setCurrentItem(1);
    }

    public void showReciently(){
        mViewPager.setCurrentItem(2);
    }
}