package com.stbig.radits.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stbig.radits.R;
import com.stbig.radits.ReproductorActivity;
import com.stbig.radits.adapters.AdapterRadios;
import com.stbig.radits.api.ApiRadios;
import com.stbig.radits.datos.AppPreferences;
import com.stbig.radits.datos.Datos;
import com.stbig.radits.helper.ManagerModel;
import com.stbig.radits.helper.PlayerConstants;
import com.stbig.radits.helper.imagen.Utils;
import com.stbig.radits.listener.ItemClickSupport;
import com.stbig.radits.objects.Radio;
import com.stbig.radits.services.APlayerService;

import java.util.ArrayList;
import java.util.Locale;

import jp.co.recruit_lifestyle.android.widget.WaveSwipeRefreshLayout;
import jp.wasabeef.recyclerview.animators.adapters.SlideInBottomAnimationAdapter;

/**
 * Created by root on 19/11/15.
 */
public class FragmentRadios extends Fragment {

    private Activity activity;
    private Context context;
    private static View view;
    private RecyclerView recyclerView;
    private AppPreferences preferences;
    private Datos data;
    private AdapterRadios adapter;
    private static final String TEXT_FRAGMENT = "TEXT_FRAGMENT";
    private WaveSwipeRefreshLayout mWaveSwipeRefreshLayout;
    private ArrayList<Radio> dataRadios;
    private Intent acpServices;
    private ManagerModel model;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity=getActivity();
        context=activity.getApplicationContext();
    }

    public static MainFragment newInstance(String text){
        MainFragment mFragment = new MainFragment();
        Bundle mBundle = new Bundle();
        mBundle.putString(TEXT_FRAGMENT, text);
        mFragment.setArguments(mBundle);
        return mFragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }

        try {
            view = inflater.inflate(R.layout.fragment_locales, null, false);
            recyclerView = (RecyclerView) view.findViewById(R.id.grid_radios);

            preferences = new AppPreferences(context);
            data= new Datos(context);
            model = new ManagerModel(context);
            acpServices = new Intent(context, APlayerService.class);

            ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                @Override
                public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                    if (data.listarRadios().get(position).getIsplaying() != 1) {

                        if (Utils.isConnectingToInternet(context)) {


                                stopRadio();
                                PlayerConstants.RADIO_STOP = false;
                                acpServices.putExtras(getRadio(position));
                                context.startService(acpServices);

                                Intent intent = new Intent(context, ReproductorActivity.class);
                                intent.putExtras(getRadio(position));
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(intent);

                        }
                    }
                }
            });

            mWaveSwipeRefreshLayout = (WaveSwipeRefreshLayout) view.findViewById(R.id.main_swipe);
            mWaveSwipeRefreshLayout.setColorSchemeColors(Color.WHITE, Color.WHITE);
            mWaveSwipeRefreshLayout.setOnRefreshListener(new WaveSwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {

                    if(Utils.isConnectingToInternet(context))
                        loadData(Locale.getDefault().toString().replace("_", "-"), "PER");
                }
            });

        } catch (InflateException e) {

        }

        return view;
    }



    @Override
    public void onResume() {
        super.onResume();

        changeModeRecycler(preferences.getValueBoolean(context.getString(R.string.mode_list)));

    }

    private Bundle getRadio(int position){
        Bundle args = new Bundle();
        args.putSerializable(getString(R.string.entity_radio), dataRadios.get(position));
        return args;
    }


    public void changeModeRecycler(boolean mode){

        dataRadios=data.listarRadios();

        adapter = new AdapterRadios(context,dataRadios,mode);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if(!mode){
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        }else{
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        }

        recyclerView.setAdapter(new SlideInBottomAnimationAdapter(adapter));
    }

    public void searchRadio(ArrayList<Radio> radios, boolean mode){

        dataRadios = radios;
        adapter = new AdapterRadios(context,dataRadios,mode);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if(!mode){
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        }else{
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        }

        recyclerView.setAdapter(new SlideInBottomAnimationAdapter(adapter));
    }


    private void loadData(final String parm2,final String parm3){
        new Thread(new Runnable() {
            @Override
            public void run() {
                ApiRadios api = new ApiRadios(context);
                boolean response = api.requestRadios("local", parm2, parm3);

                Message msg = new Message();
                msg.obj=response;
                responseWeb.sendMessage(msg);

            }
        }).start();


    }

    private Handler responseWeb = new Handler(){
        public void handleMessage(Message msg){
            mWaveSwipeRefreshLayout.setRefreshing(false);
    };
    };

    private void stopRadio(){
        model.closePlayingRadio();
        context.stopService(acpServices);
    }


}
