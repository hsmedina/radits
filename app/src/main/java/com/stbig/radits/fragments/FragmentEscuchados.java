package com.stbig.radits.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.stbig.radits.R;
import com.stbig.radits.ReproductorActivity;
import com.stbig.radits.adapters.AdapterRadios;
import com.stbig.radits.api.ApiRadios;
import com.stbig.radits.datos.AppPreferences;
import com.stbig.radits.helper.ManagerModel;
import com.stbig.radits.helper.imagen.Utils;
import com.stbig.radits.listener.ItemClickSupport;
import com.stbig.radits.objects.Radio;
import com.stbig.radits.services.APlayerService;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.Locale;

import jp.co.recruit_lifestyle.android.widget.WaveSwipeRefreshLayout;
import jp.wasabeef.recyclerview.animators.adapters.SlideInBottomAnimationAdapter;

/**
 * Created by root on 19/11/15.
 */
public class FragmentEscuchados extends Fragment {

    private Activity activity;
    private Context context;
    private static View view;
    private GridView lista;
    private AdapterRadios adapter;
    private static final String TEXT_FRAGMENT = "TEXT_FRAGMENT";
    boolean modeList = false;
    private AdView mAdViewHome;
    private AVLoadingIndicatorView indicator;
    private LinearLayout empty_content;
    private ImageView icon_empty;
    private TextView txt_empty;
    private RecyclerView recyclerView;
    private AppPreferences preferences;
    private ArrayList<Radio> dataRadios;
    private ManagerModel model;
    private WaveSwipeRefreshLayout mWaveSwipeRefreshLayout;
    private Intent acpServices;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity=getActivity();
        context=activity.getApplicationContext();
        setHasOptionsMenu(true);
    }

    public static MainFragment newInstance(String text){
        MainFragment mFragment = new MainFragment();
        Bundle mBundle = new Bundle();
        mBundle.putString(TEXT_FRAGMENT, text);
        mFragment.setArguments(mBundle);
        return mFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_escuchados, container, false);

            empty_content = (LinearLayout) view.findViewById(R.id.empty_content);
            icon_empty = (ImageView) view.findViewById(R.id.icon_empty);
            txt_empty = (TextView) view.findViewById(R.id.text_process);
            indicator = (AVLoadingIndicatorView) view.findViewById(R.id.loadingIndicatorView);
            recyclerView = (RecyclerView) view.findViewById(R.id.grid_radios);


            model = new ManagerModel(context);
            preferences = new AppPreferences(context);
            acpServices = new Intent(context, APlayerService.class);

            mAdViewHome = (AdView) view.findViewById(R.id.ads_home);
            AdRequest adRequest = new AdRequest.Builder().addTestDevice("3DD2A0D1B293710BE8CF17DCB4BB8677").build();
            mAdViewHome.loadAd(adRequest);
            mAdViewHome.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    mAdViewHome.setVisibility(View.VISIBLE);
                }
            });


            ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                @Override
                public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                    if (model.listRadioPopulares().get(position).getIsplaying() != 1) {


                        if (Utils.isConnectingToInternet(context)) {

                            stopRadio();

                            acpServices.putExtras(getRadio(position));
                            context.startService(acpServices);

                          //  Intent intent = new Intent(context, ReproductorActivity.class);
                          //  intent.putExtras(getRadio(position));
                          //  intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                          //  context.startActivity(intent);
                        }
                    }
                }
            });

            mWaveSwipeRefreshLayout = (WaveSwipeRefreshLayout) view.findViewById(R.id.main_swipe);
            mWaveSwipeRefreshLayout.setColorSchemeColors(Color.WHITE, Color.WHITE);
            mWaveSwipeRefreshLayout.setOnRefreshListener(new WaveSwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {

                    if(Utils.isConnectingToInternet(context))
                        loadRadiosPopulares(Locale.getDefault().toString().replace("_", "-"), "PER");
                }
            });

        } catch (InflateException e) {

        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if(model.listRadioPopulares().size()==0){
            empty_content.setVisibility(View.VISIBLE);
        }else{
            empty_content.setVisibility(View.GONE);
            changeModeRecycler(preferences.getValueBoolean(context.getString(R.string.mode_list)));
        }

        loadRadiosPopulares(Locale.getDefault().toString().replace("_", "-"), "PER");

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    private void loadRadiosPopulares(final String iso_lang, final String iso_country){
        new Thread(new Runnable() {
            @Override
            public void run() {
                ApiRadios pa = new ApiRadios(activity,context.getString(R.string.radits_api));
                boolean res = pa.requestRadiosPopulares(iso_lang, iso_country);

                Message msg = new Message();
                msg.obj=res;
                responseWeb.sendMessage(msg);

            }
        }).start();
    }

    private Handler responseWeb = new Handler(){
        public void handleMessage(Message msg){

            if(!Boolean.valueOf(msg.obj.toString())){
                icon_empty.setVisibility(View.VISIBLE);
            }else{

                if(model.listRadioPopulares().size()==0){
                    empty_content.setVisibility(View.VISIBLE);
                    icon_empty.setVisibility(View.VISIBLE);
                    txt_empty.setText(getString(R.string.empty_escuchados));
                }else{
                    empty_content.setVisibility(View.GONE);
                    changeModeRecycler(preferences.getValueBoolean(context.getString(R.string.mode_list)));
                }


            }

            mWaveSwipeRefreshLayout.setRefreshing(false);
            indicator.setVisibility(View.GONE);
    };
    };


    public void changeModeRecycler(boolean mode){

        dataRadios=model.listRadioPopulares();

        adapter = new AdapterRadios(context,dataRadios,mode,true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if(!mode){
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        }else{
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        }

        recyclerView.setAdapter(new SlideInBottomAnimationAdapter(adapter));

    }

    private Bundle getRadio(int position){
        Bundle args = new Bundle();
        args.putSerializable(getString(R.string.entity_radio), dataRadios.get(position));
        return args;
    }

    private void stopRadio(){
        model.closePlayingRadio();
        context.stopService(acpServices);
    }

}
