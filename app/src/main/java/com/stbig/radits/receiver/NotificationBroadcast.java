package com.stbig.radits.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.stbig.radits.MainActivity;
import com.stbig.radits.R;
import com.stbig.radits.ReproductorActivity;
import com.stbig.radits.fragments.FragmentFavoritos;
import com.stbig.radits.helper.ManagerModel;
import com.stbig.radits.helper.PlayerConstants;
import com.stbig.radits.services.APlayerService;


public class NotificationBroadcast extends BroadcastReceiver {

	private ManagerModel model;

	@Override
	public void onReceive(Context context, Intent intent) {

				model = new ManagerModel(context);

				Log.i("broadcast action", intent.getAction() + " ok ");

            	if (intent.getAction().equals(APlayerService.NOTIFY_PLAY)) {
					Log.i("broadcast play---", " ok ");
					PlayerConstants.PLAY_STOP_HANDLER.sendMessage(PlayerConstants.PLAY_STOP_HANDLER.obtainMessage(0, context.getString(R.string.play)));
        		} else if (intent.getAction().equals(APlayerService.NOTIFY_STOP)) {
					Log.i("broadcast stop---", " ok ");
					PlayerConstants.PLAY_STOP_HANDLER.sendMessage(PlayerConstants.PLAY_STOP_HANDLER.obtainMessage(0, context.getString(R.string.stop)));
        		} else if (intent.getAction().equals(APlayerService.NOTIFY_LIKE)) {
					if(!PlayerConstants.RADIO_LIKE){
						PlayerConstants.RADIO_LIKE=true;
						model.likeRadio(model.getCurrenPlayingRadio().getId(),1);
					}else{
						PlayerConstants.RADIO_LIKE=false;
						model.likeRadio(model.getCurrenPlayingRadio().getId(), 0);
					}

					PlayerConstants.RADIO_LIKE_HANDLER.sendMessage(PlayerConstants.RADIO_LIKE_HANDLER.obtainMessage());

					MainActivity.changeLikeNotification();
					FragmentFavoritos.refreshFavorites();
					//ReproductorActivity.changeLikeNotification();


        		} else if (intent.getAction().equals(APlayerService.NOTIFY_CLOSE)) {
					Intent i = new Intent(context, APlayerService.class);
					context.stopService(i);
        		}

	}

	public String ComponentName() {
		return this.getClass().getName();
	}


}
