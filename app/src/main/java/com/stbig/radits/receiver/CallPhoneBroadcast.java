package com.stbig.radits.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.stbig.radits.R;
import com.stbig.radits.ReproductorActivity;
import com.stbig.radits.helper.ManagerModel;
import com.stbig.radits.helper.PlayerConstants;

/**
 * Created by hsmedina on 7/7/2016.
 */
public class CallPhoneBroadcast extends BroadcastReceiver {


    TelephonyManager telephony;
    CustomPhoneStateListener customPhoneListener ;
    private ManagerModel model;
    private boolean stopActivo=false;
    private boolean playActivo=false;
    private static int cant=0;
    private Context context;
    @Override
    public void onReceive(Context context, Intent intent) {
        this.context=context;
        model = new ManagerModel(context);
        customPhoneListener = new CustomPhoneStateListener();
        telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        telephony.listen(customPhoneListener, PhoneStateListener.LISTEN_CALL_STATE);
    }

    public class CustomPhoneStateListener extends PhoneStateListener
    {
        private static final String TAG = "CustomPhoneStateListener";

        @Override
        public void onCallStateChanged(int state, String incomingNumber)
        {
            switch (state)
            {
                case TelephonyManager.CALL_STATE_RINGING:
                    if(!incomingNumber.equalsIgnoreCase("")){
                        Log.e("MyPhoneStateListener", "RINGING");
                        if(model.isSelected() && !stopActivo) {
                            stopActivo=true;
                            PlayerConstants.PLAY_STOP_HANDLER.sendMessage(PlayerConstants.PLAY_STOP_HANDLER.obtainMessage(0, context.getString(R.string.stop)));
                            ReproductorActivity.revelSecundary();
                        }
                    }
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    Log.e("MyPhoneStateListener", "OFFHOOK");
                    break;
                case TelephonyManager.CALL_STATE_IDLE:
                    Log.e("MyPhoneStateListener", "IDLE");
                    if(model.isSelected() && !playActivo){
                        playActivo=true;
                        PlayerConstants.PLAY_STOP_HANDLER.sendMessage(PlayerConstants.PLAY_STOP_HANDLER.obtainMessage(0, context.getString(R.string.play)));
                    }
                    break;
                default:
                    break;
            }
            super.onCallStateChanged(state, incomingNumber);
            telephony.listen(customPhoneListener, PhoneStateListener.LISTEN_NONE);
        }


    }
}