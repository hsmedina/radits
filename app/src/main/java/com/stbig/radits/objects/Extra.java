package com.stbig.radits.objects;

import java.io.Serializable;

/**
 * Created by root on 14/01/16.
 */
public class Extra implements Serializable {

    String id;
    String locale;
    String nombre;
    String type;
    String tun;
    String tun_parent;
    String iso;

    public Extra(String id, String locale, String nombre, String type, String tun, String tun_parent, String iso){
        this.id=id;
        this.locale=locale;
        this.nombre=nombre;
        this.type=type;
        this.tun=tun;
        this.tun_parent=tun_parent;
        this.iso=iso;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTun() {
        return tun;
    }

    public void setTun(String tun) {
        this.tun = tun;
    }

    public String getTun_parent() {
        return tun_parent;
    }

    public void setTun_parent(String tun_parent) {
        this.tun_parent = tun_parent;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }


}
