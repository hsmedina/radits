package com.stbig.radits.objects;

import java.io.Serializable;

/**
 * Created by root on 26/11/15.
 */
public class Radio implements Serializable {

    private String id;
    private String category;
    private String name;
    private String image;
    private String url;
    private String iso;
    private String genre;
    private String status;
    private String tunein;
    private String category_fill;
    private int favorito;
    private String reciente;
    private int isplaying;
    private int view;
    private int selected;


    public Radio(String id, String category, String name, String image, String url, String iso, String genre,
                String status, String tunein,String category_fill, int favorito, String reciente, int isplaying, int view, int selected){

        this.id=id;
        this.category=category;
        this.name=name;
        this.image=image;
        this.url=url;
        this.iso=iso;
        this.genre=genre;
        this.status=status;
        this.tunein=tunein;
        this.favorito=favorito;
        this.reciente=reciente;
        this.category_fill=category_fill;
        this.isplaying=isplaying;
        this.view=view;
        this.selected=selected;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTunein() {
        return tunein;
    }

    public void setTunein(String tunein) {
        this.tunein = tunein;
    }

    public int getFavorito() {
        return favorito;
    }

    public void setFavorito(int favorito) {
        this.favorito = favorito;
    }

    public String getReciente() {
        return reciente;
    }

    public void setReciente(String reciente) {
        this.reciente = reciente;
    }

    public String getCategory_fill() {
        return category_fill;
    }

    public void setCategory_fill(String category_fill) {
        this.category_fill = category_fill;
    }

    public int getIsplaying() {
        return isplaying;
    }

    public void setIsplaying(int isplaying) {
        this.isplaying = isplaying;
    }

    public int getView() {
        return view;
    }

    public void setView(int view) {
        this.view = view;
    }

    public int getSelected() {
        return selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }
}
