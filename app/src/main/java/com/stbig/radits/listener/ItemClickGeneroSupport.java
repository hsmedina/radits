package com.stbig.radits.listener;

import android.view.View;

import com.stbig.radits.objects.Extra;

/**
 * Created by root on 05/02/16.
 */
public interface ItemClickGeneroSupport {

    public void onSelectedListerGenero(Extra extra, View view);

}
