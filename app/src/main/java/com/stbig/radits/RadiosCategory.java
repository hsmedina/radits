package com.stbig.radits;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;
import com.stbig.radits.adapters.AdapterRadios;
import com.stbig.radits.api.ApiExtra;
import com.stbig.radits.datos.AppPreferences;
import com.stbig.radits.helper.ManagerModel;
import com.stbig.radits.helper.PlayerConstants;
import com.stbig.radits.helper.imagen.Utils;
import com.stbig.radits.listener.ItemClickSupport;
import com.stbig.radits.objects.Extra;
import com.stbig.radits.objects.Radio;
import com.stbig.radits.services.APlayerService;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import jp.co.recruit_lifestyle.android.widget.WaveSwipeRefreshLayout;
import jp.wasabeef.recyclerview.animators.adapters.SlideInBottomAnimationAdapter;

/**
 * Created by root on 11/02/16.
 */
public class RadiosCategory  extends AppCompatActivity implements  View.OnClickListener {

    private RecyclerView recyclerView;
    private ManagerModel model;
    private AdapterRadios adapter;
    private Extra extra;
    private LinearLayout empty_content;
    private AVLoadingIndicatorView indicator;
    private String idTunExtra;
    private String typeAuxExtra;
    private String isoExtra;
    private String langExtra;

    private TextView text_process;
    private AppPreferences preferences;


    private LinearLayout container;
    private ImageView img_empty;
    //sliding views
    private ImageView btnStop;
    private ImageView btnLike;
    private ImageView radioImage;
    private ImageView radioIcon;
    private TextView radioName;
    private AnimationDrawable frameAnimation;
    private boolean status_like=false;
    private ImageView animate;
    public static SlidingUpPanelLayout mediaScreen;

    private float scale;
    private int padding_70dp;

    private Radio currentRadio;
    private Intent acpServices;

    boolean modeList = false;
    private MenuItem menItemMode;

    private ArrayList<Radio> dataRadios;
    private WaveSwipeRefreshLayout mWaveSwipeRefreshLayout;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_grid);

        scale = getResources().getDisplayMetrics().density;
        padding_70dp = (int) (70 * scale + 0.5f);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

        empty_content = (LinearLayout) findViewById(R.id.empty_content);
        img_empty = (ImageView) findViewById(R.id.img_empty);
        indicator = (AVLoadingIndicatorView) findViewById(R.id.loadingIndicatorView);
        text_process = (TextView) findViewById(R.id.text_process);
        recyclerView = (RecyclerView) findViewById(R.id.grid_radios);
        recyclerView.setVisibility(View.GONE);
        //data= new Datos(this);
        model = new ManagerModel(this);
        preferences = new AppPreferences(this);

        modeList = preferences.getValueBoolean(getString(R.string.mode_list));

        container = (LinearLayout) findViewById(R.id.container);
        acpServices = new Intent(this, APlayerService.class);

        //sliding views
        btnStop = (ImageView) findViewById(R.id.stop);
        btnLike = (ImageView) findViewById(R.id.like);
        radioImage = (ImageView) findViewById(R.id.image_radio);
        radioIcon = (ImageView) findViewById(R.id.icon_radio);
        radioName = (TextView) findViewById(R.id.song_name);
        animate = (ImageView)  findViewById(R.id.play_animation);
        mediaScreen = (SlidingUpPanelLayout) findViewById(R.id.media_screen);

        btnStop.setOnClickListener(this);
        btnLike.setOnClickListener(this);

        mediaScreen.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);

        mWaveSwipeRefreshLayout = (WaveSwipeRefreshLayout) findViewById(R.id.main_swipe);
        mWaveSwipeRefreshLayout.setColorSchemeColors(Color.WHITE, Color.WHITE);

    }

    @Override
    protected void onResume() {
        super.onResume();

        frameAnimation = (AnimationDrawable) animate.getBackground();

        if(model.isSelected()){
            showCollapsePlayer();
        }else{
            hiddeCollapsePlayer();
        }

        registerReceiver(trackingServiceRadio, new IntentFilter(APlayerService.RADIO_STATUS_START));

        loadData();

        mWaveSwipeRefreshLayout.setOnRefreshListener(new WaveSwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if(Utils.isConnectingToInternet(RadiosCategory.this)){
                    img_empty.setVisibility(View.GONE);
                    if(!typeAuxExtra.equals("country")){
                        loadRadiosCategory(idTunExtra);
                    }else {
                        loadRadiosPais(langExtra, isoExtra, idTunExtra);
                    }
                }

            }
        });

    }

    private void loadData() {

        Bundle argsExtra = getIntent().getExtras();

        if(argsExtra!=null){
            extra = (Extra) argsExtra.get(getString(R.string.entity_category));
            getSupportActionBar().setTitle(extra.getNombre());
            idTunExtra = extra.getTun();
            typeAuxExtra = extra.getType();
            isoExtra = extra.getIso();
            langExtra = extra.getLocale();

            if(!typeAuxExtra.equals("country")){
                categoryRadios();
            }else {
                paisesRadios();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        menItemMode = menu.findItem(R.id.mode);

        if(preferences.getValueBoolean(getString(R.string.mode_list))){
            menItemMode.setIcon(getResources().getDrawable(R.drawable.ic_grid));
        }else{
            menItemMode.setIcon(getResources().getDrawable(R.drawable.ic_linear));
        }

        SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();


        SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextChange(String newText){

                if(newText.length()==0){
                    if(!typeAuxExtra.equals("country")){
                        setupRecycler(model.listRadioCategory(idTunExtra), modeList);
                    }else{
                        setupRecycler(model.listRadioPaises(isoExtra), modeList);
                    }
                }

                return true;
            }
            @Override
            public boolean onQueryTextSubmit(String query){
                if(!typeAuxExtra.equals("country")){
                    setupRecycler(model.listRadioCategorySearch(idTunExtra, query), modeList);
                }else{
                    setupRecycler(model.listRadioPaisesSearch(isoExtra, query), modeList);
                }

                return true;
            }

        };
        searchView.setOnQueryTextListener(textChangeListener);
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {

                if(!typeAuxExtra.equals("country")){
                    setupRecycler(model.listRadioCategory(idTunExtra), modeList);
                }else{
                    setupRecycler(model.listRadioPaises(isoExtra), modeList);
                }

                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.mode:
                modeList = !modeList;

                if(!typeAuxExtra.equals("country")){
                    setupRecycler(model.listRadioCategory(idTunExtra), modeList);
                }else{
                    setupRecycler(model.listRadioPaises(isoExtra), modeList);
                }

                preferences.saveValueBoolean(getString(R.string.mode_list), modeList);
                if(!modeList) {
                    item.setIcon(getResources().getDrawable(R.drawable.ic_linear));
                }else{
                    item.setIcon(getResources().getDrawable(R.drawable.ic_grid));
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }




    private void categoryRadios(){
        if(model.listRadioCategory(idTunExtra).size()>0){
            setupRecycler(model.listRadioCategory(idTunExtra), modeList);
        }else{
            if(Utils.isConnectingToInternet(this))
                loadRadiosCategory(idTunExtra);
        }
    }

    private void paisesRadios(){
        if(model.listRadioPaises(isoExtra).size()>0){
            setupRecycler(model.listRadioPaises(isoExtra), modeList);
        }else{
            if(Utils.isConnectingToInternet(this))
                loadRadiosPais(langExtra, isoExtra, idTunExtra);
        }
    }

    private void loadRadiosCategory(final String id_category){

        indicator.setVisibility(View.VISIBLE);
        text_process.setText(getString(R.string.load_radio));
        new Thread(new Runnable() {
            @Override
            public void run() {
                ApiExtra pa = new ApiExtra(RadiosCategory.this);
                boolean res = pa.requestRadiosByCategory("bygenre", id_category);

                Message msg = new Message();
                msg.obj=res;
                responseWeb.sendMessage(msg);

            }
        }).start();

    }

    private void loadRadiosPais(final String localeLang,final String isoCountry,final String id_category){

        indicator.setVisibility(View.VISIBLE);
        text_process.setText(getString(R.string.load_radio));
        new Thread(new Runnable() {
            @Override
            public void run() {

                ApiExtra pa = new ApiExtra(RadiosCategory.this);
                boolean res = pa.requestRadiosByCountry("local", localeLang, isoCountry, id_category);

                Message msg = new Message();
                msg.obj=res;
                responseWeb.sendMessage(msg);

            }
        }).start();

    }


    private Handler responseWeb = new Handler(){
        public void handleMessage(Message msg){

            if(!Boolean.valueOf(msg.obj.toString())){
                Toast.makeText(RadiosCategory.this, "download failed!", Toast.LENGTH_SHORT).show();
            }

            if(!typeAuxExtra.equals("country")){
                setupRecycler(model.listRadioCategory(idTunExtra), modeList);
            }else{
                setupRecycler(model.listRadioPaises(isoExtra), modeList);
            }

            mWaveSwipeRefreshLayout.setRefreshing(false);
            indicator.setVisibility(View.GONE);
        };
    };


    private void setupRecycler(ArrayList<Radio> radios, boolean modeList){

        dataRadios = radios;

        adapter = new AdapterRadios(this,dataRadios,modeList);

        if(adapter.getItemCount()>0){
            recyclerView.setItemAnimator(new DefaultItemAnimator());

            if(!modeList) {
                recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
            }else{
                recyclerView.setLayoutManager(new LinearLayoutManager(this));
            }

            recyclerView.setAdapter(new SlideInBottomAnimationAdapter(adapter));

            ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                @Override
                public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                    if (dataRadios.get(position).getIsplaying() != 1) {

                        stopRadio();
                        PlayerConstants.RADIO_STOP = false;
                        acpServices.putExtras(getRadio(position));
                        startService(acpServices);

                        Intent intent = new Intent(RadiosCategory.this, ReproductorActivity.class);
                        intent.putExtras(getRadio(position));
                        startActivity(intent);
                    }
                }
            });
            recyclerView.setVisibility(View.VISIBLE);
            empty_content.setVisibility(View.GONE);
            img_empty.setVisibility(View.GONE);
        }else{
            recyclerView.setVisibility(View.GONE);
            img_empty.setVisibility(View.VISIBLE);
            empty_content.setVisibility(View.VISIBLE);
            text_process.setText(getString(R.string.empty_radios) + " " + extra.getNombre());
        }
    }


    private Bundle getRadio(int position){
        Bundle args = new Bundle();
        args.putSerializable(getString(R.string.entity_radio), dataRadios.get(position));
        args.putSerializable(getString(R.string.entity_category), extra);
        return args;
    }



    private void stopRadio(){
        model.closePlayingRadio();
        stopService(acpServices);
        hiddeCollapsePlayer();
    }

    private void likeRadio(){
        if(!status_like){
            status_like=true;
            //data.setLikeRadio(currentRadio.getId(), 1);
            model.likeRadio(currentRadio.getId(), 1);
            Utils.animateLike(btnLike);
        }else{
            status_like=false;
            //data.setLikeRadio(currentRadio.getId(),0);
            model.likeRadio(currentRadio.getId(), 0);
            Utils.animateUnLike(btnLike);
        }

    }

    private void loadInfoRadio(){
        //currentRadio = data.getPlayingRadio();
        currentRadio = model.getCurrenPlayingRadio();
        if(currentRadio!=null) {
            radioName.setText(currentRadio.getName());
            if(currentRadio.getFavorito()==0) {
                status_like = false;
                btnLike.setImageResource(R.drawable.ic_like_off);
            }else {
                status_like = true;
                btnLike.setImageResource(R.drawable.ic_like_on);
            }

            Picasso.with(this)
                    .load(currentRadio.getImage())
                    .resize(Utils.dpToPx(this,250), Utils.dpToPx(this,250))
                    .centerCrop()
                    .into(radioImage);

            Picasso.with(this)
                    .load(currentRadio.getImage())
                    .resize(Utils.dpToPx(this,35), Utils.dpToPx(this,35))
                    .centerCrop()
                    .into(radioIcon);

        }


    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.stop:
                stopRadio();
                break;
            case R.id.like:
                likeRadio();
                break;
        }

    }


    @Override
    public void onBackPressed() {

        if(mediaScreen.getPanelState().equals(SlidingUpPanelLayout.PanelState.EXPANDED)){
            mediaScreen.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        }else{
            super.onBackPressed();
        }

    }

    private BroadcastReceiver trackingServiceRadio = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub

            int status = intent.getExtras().getInt(getString(R.string.radio_status));

            switch(status){
                case -1:
                    Toast.makeText(RadiosCategory.this, getString(R.string.msg_error_server),Toast.LENGTH_SHORT).show();
                    stopRadio();
                    break;
                case 1:
                    showCollapsePlayer();
                    break;
                case 2:
                    hiddeCollapsePlayer();
                    break;

            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("onDestroy","destroy activity");
        //stopRadio();
        unregisterReceiver(trackingServiceRadio);
    }

    private void showCollapsePlayer(){
        loadInfoRadio();
        frameAnimation.start();
        mediaScreen.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        container.setPadding(0, 0, 0, padding_70dp);
    }

    private void hiddeCollapsePlayer(){
        frameAnimation.stop();
        mediaScreen.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        container.setPadding(0, 0, 0, 0);
    }

}
